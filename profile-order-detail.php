<?php include '_header.php' ?>

<div class="page page-profile page-order-detail">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      
      <?php $active = 'profile-order-history'; include '_profile-sidebar.php';?>

      <div class="page-content">
        <h1 class="page-title">CHI TIẾT ĐƠN HÀNG</h1>

        <div class="content-wrapper">
          
          <div class="order-detail-header rounded-section">
            <div class="custom-row">
              <div class="column-3">
                Đơn hàng #396113597
              </div>
              <div class="column-3 text-center">
                Đặt ngày 15/08/2017
              </div>
              <div class="column-3 text-right">
                <b class="fw500">Tổng tiền: 219.000 VND</b>
              </div>
            </div>
          </div>

          <div class="rounded-section progress-section">
            <div class="custom-row">
              <div class="pull-left">Giao ngày 16/08/2017 bởi <b>Hà Linh</b></div>
              <div class="pull-right">
                Được bán bởi <a href="">Thành Long quán</a>
              </div>
            </div>

            <div class="progress-wrapper">
              
              <!-- Add or remove class active for current step -->
              <div class="progress-item active">
                <div class="progress-item-wrapper">
                  <i class="icon-reload"></i>
                  <span>Đang xử lý</span>
                </div>
              </div>
              
              <!-- Add or remove class active for current step -->
              <div class="progress-item active">
                <div class="progress-item-wrapper">
                  <i class="icon-moto"></i>
                  <span>Đã được gửi đi</span>
                </div>
              </div>
              
              <!-- Add or remove class active for current step -->
              <div class="progress-item">
                <div class="progress-item-wrapper">
                  <i class="icon-check-circle"></i>
                  <span>Đã giao hàng</span>
                </div>
              </div>

            </div>

            <div class="progress-products">
              <table class="table">
                <tr>
                  <td width="106"><img src="img/product-1.png" alt="" class="img-responsive"></td>
                  <td width="40%">Chicken Go Saigon - Shop Online</td>
                  <td width="20%">219.000 VND</td>
                  <td>x 1</td>
                </tr>
              </table>
            </div>
          </div>
          
          <!-- Status Cancel -->
          <div class="rounded-section progress-section">
            <div class="custom-row">
              <div class="pull-left">Giao ngày 16/08/2017 bởi <b>Hà Linh</b></div>
              <div class="pull-right">
                Được bán bởi <a href="">Thành Long quán</a>
              </div>
            </div>

            <div class="progress-wrapper">
              
              <!-- Add or remove class active for current step -->
              <div class="progress-item cancel">
                <div class="progress-item-wrapper">
                  <i class="icon-close-circle"></i>
                  <span>đã hủy</span>
                </div>
              </div>
              
              <!-- Add or remove class active for current step -->
              <div class="progress-item">
                <div class="progress-item-wrapper">
                  <i class="icon-moto"></i>
                  <span>Đã được gửi đi</span>
                </div>
              </div>
              
              <!-- Add or remove class active for current step -->
              <div class="progress-item">
                <div class="progress-item-wrapper">
                  <i class="icon-check-circle"></i>
                  <span>Đã giao hàng</span>
                </div>
              </div>

            </div>

            <div class="progress-products">
              <table class="table">
                <tr>
                  <td width="106"><img src="img/product-1.png" alt="" class="img-responsive"></td>
                  <td width="40%">
                    Chicken Go Saigon - Shop Online
                    <button type="button" class="btn btn-cancelled">Đã hủy</button>
                  </td>
                  <td width="20%">219.000 VND</td>
                  <td>x 1</td>
                </tr>
              </table>
            </div>
          </div>

          <div class="rounded-section summary">
            <h4 class="header">Tóm tắt</h3>
            <div class="custom-row">
              <div class="pull-left">Tạm tính</div>
              <div class="pull-right">219.000 VND</div>
            </div>
            <div class="custom-row">
              <div class="pull-left">Phí vận chuyển</div>
              <div class="pull-right">0 VND</div>
            </div>
            <div class="custom-row">
              <div class="pull-left">Khuyến mãi</div>
              <div class="pull-right">0 VND</div>
            </div>

            <hr>

            <div class="custom-row">
              <div class="pull-left">
                Tổng (Đã bao gồm VAT.)
                <div class="small">Thanh toán bằng Thanh toán khi nhận hàng</div>
              </div>
              <div class="pull-right"><b class="red">219.000 VND</b></div>
            </div>
          </div>

          <div class="order-detail-footer">
          <div class="rounded-section shipping-address">
            <h4 class="header">Địa chỉ giao hàng</h3>
            <p class="name">Nguyễn Minh Toàn</p>
            <p>81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM</p>
            <p>0969740960</p>
          </div>

          <div class="order-detail-footer">
            <div class="custom-row">
              <div class="pull-left">
                <a href="" class="link-remove">Hủy đơn hàng</a>
              </div>
              <div class="pull-right">
                <a href="profile-order-history.html" class="btn btn-normal">CÁC ĐƠN HÀNG KHÁC</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_footer.php' ?>