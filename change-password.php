<?php include '_header.php' ?>

<div class="page page-profile">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      <?php include '_profile-sidebar.php';?>

      <div class="page-content">
        <h1 class="page-title">ĐỔI MẬT KHẨU</h1>

        <div class="content-wrapper profile-wrapper">

          <form action="" class="form-horizontal">
            
            <!-- Old Password -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Mật khẩu cũ:</label>
              <div class="col-xs-5">
                <input type="password" class="form-control">
              </div>
            </div>
            
            <!-- New Password -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Mật khẩu mới</label>
              <div class="col-xs-5">
                <input type="password" class="form-control">
              </div>
            </div>
            
            <!-- Confirm Password -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Nhập lại mật khẩu</label>
              <div class="col-xs-5">
                <input type="password" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <div class="col-xs-4"></div>
              <div class="col-xs-5 text-right">
                <button class="btn btn-primary" type="submit">Lưu thay đổi</button>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_footer.php' ?>