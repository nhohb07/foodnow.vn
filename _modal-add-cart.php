<div class="modal fade modal-header-transparent user-list-modal add-cart-modal" id="addCartModal" tabindex="-1" role="dialog" aria-labelledby="addCartModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="header">
            <h2>Whisk - Bánh Cheese Tart - Bitexco Tower</h2>
            <span class="store-name">TIỆM BÁNH - CHÂU Á</span>
            <span class="store-address">83/35 Phạm Văn Bạch, P. 15, Quận 10, TP.HCM</span>
          </div>
          <div class="detail">
            <div class="detail-content">

              <div class="row">
                <div class="col-xs-3">
                  <img src="img/product-1.png" alt="" class="img-responsive" id="product-image">
                </div>
                <div class="col-xs-9">
                  <div class="product-name">Gà ta bó xôi truyền thống</div>
                  <div class="product-desc">Nguyên con</div>
                  <div class="product-price">Giá: 359,000 đ</div>
                </div>
              </div>

              <div class="add-more">
                <p class="help">Thêm <span>(Chọn tối đa 1 món)</span></p>

                <ul class="add-more-list">
                  <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
                  <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
                  <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
                  <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
                  <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
                  <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
                </ul>
              </div>

              <div class="display-table">
                <div class="table-td">
                  <div class="product-quantity">
                    <span class="quantity-button quantity-decrease"><i class="icon-minus"></i></span>
                    <span class="quantity-text">1</span>
                    <span class="quantity-button quantity-increase"><i class="icon-plus"></i></span>
                  </div>
                </div>
                <div class="table-td text-right">
                  <button class="btn button-submit" onclick="flyToElement('#product-image', '#cart-fly-here');$('#addCartModal').modal('hide')">ĐỒNG Ý +359,000 Đ</button>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
