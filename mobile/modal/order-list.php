<div class="modal fade modal-order-list" id="modalOrderList" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Chi tiết đơn hàng'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">
        
        <form class="form-horizontal">
          <div class="order-search">
            <div class="row row-small-space">
              <label class="col-xs-4 control-label">Trạng thái</label>
              <div class="col-xs-8">
                <select class="form-control">
                  <option value="">Tất cả</option>
                  <option value="1">Đã thanh toán</option>
                  <option value="2">Đã giao hàng</option>
                </select>
              </div>
            </div>

            <div class="row row-small-space">
              <div class="col-xs-6">
                <div class="row row-small-space">
                  <label class="col-xs-3 control-label">Từ</label>
                  <div class="col-xs-9">
                    <input type="text" class="form-control datepicker" value="01/07/2017">
                  </div>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="row row-small-space">
                  <label class="col-xs-3 control-label text-right">đến</label>
                  <div class="col-xs-9">
                    <input type="text" class="form-control datepicker" value="31/07/2017">
                  </div>
                </div>
              </div>
            </div>

            <button class="btn btn-block btn-other rounded btn-small btn-normal">Tìm</button>
          </div>
        </form>

        <div class="order-list">

          <?php for($i = 0; $i < 4; $i++) { ?>
          <div class="order-item">
            
            <div class="custom-row order-item-row">
              <div class="pull-left">
                <b>Đơn hàng #3454657</b>
              </div>
              <div class="pull-right">
                <span class="text-gray">Đã giao hàng</span>
              </div>
            </div>

            <div class="order-item-row">
              <p>Đặt món: 22/10/2017 10:00</p>
              <p>Giao hàng: 22/10/2017 10:00</p>
            </div>

            <div class="store-info order-item-row">
              <div><b>Cửa hàng bánh Gia Phúc</b></div>
              <div class="text-gray">360 Lý Thái Tổ, P. 1, Quận 3, TP. HCM</div>
            </div>

            <div class="order-item-row">
              <div class="text-red"><b>219.000 VND</b></div>
              <div class="text-gray">13 món (Giao hàng nhận tiền)</div>
            </div>

            <div class="row row-small-space">
              <div class="col-xs-4">
                <a href="" class="text-blue">Đặt hàng lại</a>
              </div>
              <div class="col-xs-4">
                <a href="" class="text-blue">In hóa đơn</a>
              </div>
              <div class="col-xs-4">
                <a onclick="modalOpenOtherModal('#modalOrderList', '#modalOrderDetail')" class="text-blue">
                  Xem chi tiết
                </a>
              </div>
            </div>

          </div>
          <?php } ?>

        </div>

      </div>
    </div>
  </div>
</div>