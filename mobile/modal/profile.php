<div class="modal fade modal-profile" id="modalProfile" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'THÔNG TIN CÁ NHÂN'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">
        <form action="" data-toggle="validator">
          <div class="form-body">
            <!-- Email -->
            <div class="form-group text-center">
              <a>
                <img src="../img/icon-avatar.png" alt="" class="img-responsive center-block icon-avatar" width="80">
                <span class="avatar-text">Ảnh đại diện</span>
              </a>
            </div>
              
            <div class="form-group">
              <label class="control-label">Địa chỉ email:</label>
              <input type="email" value="weedsbrand@gmail.com" class="form-control" required data-error="Hãy nhập email">
              <div class="help-block with-errors"></div>
            </div>
              
            <!-- Phone -->
            <div class="form-group">
              <label class="control-label">Điện thoại di động</label>
              <input type="text" class="form-control" placeholder="Không có">
            </div>
              
            <!-- Name -->
            <div class="form-group">
              <label class="control-label">Tên</label>
              <input type="text" value="Nguyễn Toàn" class="form-control" data-error="Hãy nhập tên" required>
              <div class="help-block with-errors"></div>
            </div>

            <!-- Gender -->
            <div class="form-group">
              <label class="control-label">Giới tính</label>
              <select class="form-control">
                <option value="">Chọn</option>
                <option value="0">Nữ</option>
                <option value="1">Nam</option>
              </select>
            </div>

            <!-- Birthday -->
            <div class="form-group">
              <label class="control-label">Ngày sinh</label>
              <div class="row row-small-space">
                <div class="col-xs-4">
                  <select class="form-control">
                    <option value="">Ngày</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>
                </div>
                <div class="col-xs-4">
                  <select class="form-control">
                    <option value="">Tháng</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>
                </div>
                <div class="col-xs-4">
                  <select class="form-control">
                    <option value="">Năm</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                  </select>
                </div>
              </div>
            </div>

          </div>

          <div class="form-footer">
            <a class="link-change-password" onclick="modalOpenOtherModal('#modalProfile', '#modalChangePassword')">Đổi mật khẩu</a>

            <button class="btn btn-block btn-green btn-normal" type="submit">Lưu thay đổi</button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>