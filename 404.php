<?php $fileName = basename($_SERVER['PHP_SELF'], '.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <title>www.foodnow.vn</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="lib/OwlCarousel/owl.carousel.min.css">
  <link rel="stylesheet" href="lib/OwlCarousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="lib/toastr/toastr.min.css">
  <link rel="stylesheet" href="lib/jquery-ui-1.12.1/jquery-ui.min.css">
  <link rel="stylesheet" href="css/<?=$fileName?>.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
  <script src="lib/toastr/toastr.min.js"></script>
  </head>
  <body>
    <div class="page-404">
      <div class="img-container">
        <img src="img/404.png" alt="">
      </div>
    </div>
  </body>
</html>