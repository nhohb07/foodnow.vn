<?php include '_header.php' ?>

<section class="all-modal">
  <button class="btn btn-green btn-small btn-normal" data-toggle="modal" data-target="#modalWelcome">Xin chào!</button>
  <button class="btn btn-green btn-small btn-normal" data-toggle="modal" data-target="#modalMaintain">Xin lỗi! Hệ thống đang bảo trì</button>
  <button class="btn btn-green btn-small btn-normal" data-toggle="modal" data-target="#modalCongration">Chúc mừng bạn đã đặt món...</button>
  <button class="btn btn-green btn-small btn-normal" data-toggle="modal" data-target="#modalInfoWhite">Chào bạn đã đến... (header white)</button>
  <button class="btn btn-green btn-small btn-normal" data-toggle="modal" data-target="#modalInfo">Chào bạn đã đến... (header gray)</button>
</section>


<!-- Welcome -->
<div class="modal fade modal-auto modal-header-img modal-welcome" id="modalWelcome" tabindex="-1" role="dialog">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-heading-img">
          <img src="../img/modal-info-1-header.png" alt="" class="img-responsive">
        </div>

        <div class="modal-body scrollable">
          <div class="info-modal-content">
            <h5>Xin chào!</h5>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn btn-block btn-small btn-green btn-normal" data-dismiss="modal">Đóng</button>
          </div>
          
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Maintain -->
<div class="modal fade modal-auto modal-header-img modal-maintain" id="modalMaintain" tabindex="-1" role="dialog">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-heading-img">
          <img src="../img/maintain.png" alt="" class="img-responsive">
        </div>

        <div class="modal-body scrollable">
          <div class="info-modal-content">
            <h5>Xin lỗi! Hệ thống đang bảo trì</h5>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn btn-block btn-small btn-green btn-normal" data-dismiss="modal">Đóng</button>
          </div>
          
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Congration -->
<div class="modal fade modal-auto modal-header-img modal-congration" id="modalCongration" tabindex="-1" role="dialog">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-heading-img">
          <img src="../img/done-bg.png" alt="" class="img-responsive center-block">
        </div>

        <div class="modal-body scrollable">
          <div class="info-modal-content">
            <h5>Chúc mừng bạn đã đặt món thành công!</h5>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn btn-block btn-small btn-green btn-normal" data-dismiss="modal">Đóng</button>
          </div>
          
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Info (header gray) -->
<div class="modal fade modal-auto modal-header-img modal-info" id="modalInfo" tabindex="-1" role="dialog">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title text-left">Thông báo</h4>
        </div>

        <div class="modal-body scrollable">
          <div class="info-modal-content">
            <h5>Xin lỗi! Hệ thống đang bảo trì</h5>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn btn-block btn-small btn-green btn-normal" data-dismiss="modal">Đóng</button>
          </div>
          
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Info (header white) -->
<div class="modal fade modal-auto modal-header-img modal-info modal-info-white" id="modalInfoWhite" tabindex="-1" role="dialog">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title text-left">Thông báo</h4>
        </div>

        <div class="modal-body scrollable">
          <div class="info-modal-content">
            <h5>Xin lỗi! Hệ thống đang bảo trì</h5>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn btn-block btn-small btn-green btn-normal" data-dismiss="modal">Đóng</button>
          </div>
          
        </div>

      </div>
    </div>
  </div>
</div>


<?php include '_footer.php' ?>