<?php include '_header.php' ?>

<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyAwTlz1kBY5NKHVJaMZ2jnqqy0-PMhA0P4"></script>

<div class="page page-profile page-profile-address">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      <?php $active = 'profile-address'; include '_profile-sidebar.php';?>

      <div class="page-content">
        <h1 class="page-title">Sổ địa chỉ của bạn</h1>

        <div class="content-wrapper">

          <ul>
            <li>
              <label class="radio-wrapper radio-default">
                <input type="radio" id="address-0" name="address" checked>
                <span></span>
              </label>
              <div class="address-info">
                <label for="address-0">Chọn địa chỉ giao hàng</label>
                <div class="address-detail">
                  <div class="pull-left">
                    <div class="address-name">Nguyễn Minh Toàn</div>
                    <div class="address-phone">
                      81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                      0969740960
                    </div>
                  </div>
                  <div class="pull-right">
                    <a class="link link-edit">Sửa</a>
                    <a class="link link-remove">Xóa</a>
                  </div>
                </div>
              </div>
            </li>

            <li>
              <label class="radio-wrapper radio-default">
                <input type="radio" id="address-1" name="address">
                <span></span>
              </label>
              <div class="address-info">
                <label for="address-1">Chọn địa chỉ giao hàng</label>
                <div class="address-detail">
                  <div class="pull-left">
                    <div class="address-name">Nguyễn Minh Toàn</div>
                    <div class="address-phone">
                      81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                      0969740960
                    </div>
                  </div>
                  <div class="pull-right">
                    <a class="link link-edit">Sửa</a>
                    <a class="link link-remove">Xóa</a>
                  </div>
                </div>
              </div>
            </li>
          </ul>

          <div class="actions">
            <a class="link link-edit" data-toggle="modal" data-target="#addAddressModal">Thêm địa chỉ mới</a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_modal-add-address.php' ?>
<?php include '_footer.php' ?>