<div class="modal fade modal-header-transparent add-address-modal" id="addAddressModal" tabindex="-1" role="dialog" aria-labelledby="addAddressModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-body">

          <h2 class="modal-heading">THÊM ĐỊA CHỈ GIAO HÀNG</h2>

          <div class="modal-content-wrapper">
            
            <form action="">

              <div class="row">
                <div class="col-xs-6">

                  <div class="form-group">
                    <label>Họ tên người nhận hàng</label>
                    <input type="text" class="form-control" placeholder="Họ và Tên">
                  </div>

                  <div class="form-group">
                    <label>Địa chỉ nhận hàng</label>
                    <input type="text" class="form-control" placeholder="Địa chỉ">
                  </div>
          
                  <div class="form-group">
                    <select name="" class="form-control">
                      <option value="" disabled hidden selected>Chọn tỉnh / thành phố</option>
                      <option value="1">Hồ Chí Minh</option>
                      <option value="2">Hà Nội</option>
                    </select>
                  </div>
          
                  <div class="form-group">
                    <select name="" class="form-control">
                      <option value="" disabled hidden selected>Chọn quận / huyện</option>
                      <option value="1">Quận 1</option>
                      <option value="2">Quận 2</option>
                    </select>
                  </div>
          
                  <div class="form-group">
                    <select name="" class="form-control">
                      <option value="" disabled hidden selected>Chọn phường / xã</option>
                      <option value="1">Phường 1</option>
                      <option value="2">Phường 2</option>
                    </select>
                  </div>
          
                </div>

                <div class="col-xs-6">

                  <div class="form-group">
                    <label>&nbsp;</label>
                    <input type="text" class="form-control" placeholder="Số điện thoại">
                  </div>

                  <div id="map-canvas" class="map-container"></div>
                  
                </div>
              </div>

              <div class="form-group">
                <label>Ghi chú địa chỉ</label>
                <textarea type="text" class="form-control" placeholder="Ghi chú thêm giúp người giao hàng tìm dễ dàng..."></textarea>
              </div>

              <label class="radio-default">
                <input type="radio" id="is-default" name="address">
                <span>Sử dụng địa chỉ này làm địa chỉ mặc định</span>
              </label>

              <div class="actions text-right">
                <button type="button" class="btn btn-default btn-normal" data-dismiss="modal">Hủy</button>
                <button type="submit" class="btn btn-primary btn-submit">Lưu thay đổi</button>
              </div>

            </form>
            
          </div>

        </div>
      </div>
    </div>
  </div>
</div>


<script>
  $(document).ready(function(){
    $('#addAddressModal').on('shown.bs.modal',function(){
      initialize();
    });
  });

  function initialize() {
    var map_canvas = document.getElementById('map-canvas');
    var map_options = {
      center: new google.maps.LatLng(10.762622, 106.660172),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options)
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>