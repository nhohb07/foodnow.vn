<div class="modal fade modal-forgot-password" id="modalActivePin" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'xác nhận'; include 'modal-header.php'; ?>

      <div class="modal-body">
        <div class="form-icon">
          <img src="../img/icon-active-pin.png" alt="" class="img-responsive center-block" width="70">
        </div>

        <div class="text-help">
          Hãy nhập vào Mã xác nhận trong email hoặc số điện thoại đã đăng ký trước đó để nhận lại mật khẩu mới.
        </div>

        <form action="" data-toggle="validator" onsubmit="return modalOpenOtherModal('#modalActivePin', '#modalEnterNewPassword')">
          <div class="form-group">
            <input type="text" class="form-control rounded shadow" data-error="Hãy nhập mã xác nhận" required placeholder="Mã xác nhận">
            <div class="help-block with-errors"></div>
          </div>

          <button type="submit" class="btn-green btn-block">XÁC NHẬN</button>
        </form>
      </div>
    </div>
  </div>
</div>
