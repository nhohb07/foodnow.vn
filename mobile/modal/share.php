<div class="modal fade modal-share" id="modalShare" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Chia sẻ cho nhóm'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">

        <h5 class="modal-heading">Copy link và gửi cho nhóm</h5>
        
        <div class="input-link">
          <input type="text" readonly class="share-link" value="https://www.deliverynow.vn/ho-chi-minh/chicken-go-saigon-shop-o" onclick="copyShareLink(this)">
          <span id="copy-status" style="display: none;"><i class="icon-check"></i> Copied</span>
        </div>

        <div class="share-help share-action">
          <span>Chia sẻ qua:</span>
          <a href="" target="_blank"><img src="../img/icon-facebook-circle.png" alt=""></a>
          <a href="" target="_blank"><img src="../img/icon-email-circle.png" alt=""></a>
        </div>

      </div>
    </div>
  </div>
</div>

<script>
  function copyShareLink(element) {
    $(element).select();
    copyToClipboard($(element).val(), this);
    $('#copy-status').fadeIn();
    setTimeout(function() {
      $('#copy-status').fadeOut();
    }, 3000);
  }
</script>