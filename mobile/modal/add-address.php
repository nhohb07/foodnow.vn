<div class="modal fade modal-auto modal-add-address" id="modalAddAddress" tabindex="-1" role="dialog">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title text-left modal-title-normal">Thêm mới địa điểm nhận hàng</h4>
        </div>

        <div class="modal-body">
          <form action="" data-toggle="validator">
            <!-- Name -->
            <div class="form-group">
              <input type="text" class="form-control input-line-bottom" placeholder="Họ và Tên" value="Nguyễn Minh Toàn" required data-error="Hãy nhập họ tên">
              <div class="help-block with-errors"></div>
            </div>

            <!-- Phone -->
            <div class="form-group">
              <input type="text" class="form-control input-line-bottom" placeholder="Số điện thoại" value="0912345678" required data-error="Hãy nhập số điện thoại">
              <div class="help-block with-errors"></div>
            </div>
    
            <!-- Address -->
            <div class="form-group">
              <input type="text" class="form-control input-line-bottom" placeholder="Tầng, số nhà, đường,..." required data-error="Hãy nhập địa chỉ">
              <div class="help-block with-errors"></div>
            </div>

            <!-- Ward, District -->
            <div class="row row-small-space">
              <div class="col-xs-6">
                <!-- Ward -->
                <div class="form-group">
                  <select name="" class="form-control input-line-bottom" required data-error="Hãy chọn phường/xã">
                    <option value="" disabled hidden selected>Phường/xã</option>
                    <option value="1">Phường 1</option>
                    <option value="2">Phường 2</option>
                  </select>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-xs-6">
                <!-- District -->
                <div class="form-group">
                  <select name="" class="form-control input-line-bottom" required data-error="Hãy chọn quận/huyện">
                    <option value="" disabled hidden selected>Quận/huyện</option>
                    <option value="1">Quận 1</option>
                    <option value="2">Quận 2</option>
                  </select>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>
    
            <!-- City -->
            <div class="form-group">
              <select name="" class="form-control input-line-bottom" required data-error="Hãy chọn tỉnh/Thành phố">
                <option value="" disabled hidden selected>Tỉnh/Thành phố</option>
                <option value="1">Hồ Chí Minh</option>
                <option value="2">Hà Nội</option>
              </select>
              <div class="help-block with-errors"></div>
            </div>

            <!-- Actions -->
            <div class="row row-small-space">
              <div class="col-xs-6">
                <button type="button" data-dismiss="modal" class="btn rounded btn-other btn-block btn-small">hủy</button>
              </div>
              <div class="col-xs-6">
                <button type="submit" class="btn btn-green btn-block btn-small">lưu lại</button>
              </div>
            </div>

          </form>
          
        </div>

      </div>
    </div>
  </div>
</div>