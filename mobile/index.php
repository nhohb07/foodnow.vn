<?php include '_header.php' ?>
  
  <section class="banner">
    <div class="city-selection">
      <button data-toggle="modal" data-target="#modalCitySelection">TP Hồ Chi Minh <b class="caret"></b></button>
    </div>

    <h1><b>THƯỢNG ĐẾ!</b> Hãy gọi món đi ạ.</h1>

    <div class="search-box">
      <form action="">
        <input type="text" placeholder="Tìm món ưa thích ngay...">
        <button><i class="icon-search"></i></button>
      </form>
    </div>
  </section>

  <section class="slider">
    <div class="row row-small-space">
      <div class="col-xs-4">
        <button onclick="initHomeOwlSlider()" class="btn rounded btn-block btn-yellow">Món sáng</button>
      </div>

      <div class="col-xs-4">
        <button onclick="initHomeOwlSlider()" class="btn rounded btn-block btn-red">Món xế</button>
      </div>

      <div class="col-xs-4">
        <button onclick="initHomeOwlSlider()" class="btn rounded btn-block btn-gray">Món tối</button>
      </div>
    </div>
    
    <button onclick="initHomeOwlSlider()" class="btn btn-block btn-package">
      hàng đóng gói
      <i class="icon-package"></i>
    </button>

    <div class="owl-carousel" id="home-product-slider">
    </div>
  </section>

  <section class="most-order">
    <h2 class="section-header">Đang được đặt nhiều</h2>

    <div class="row row-small-space row-buttons">
      <div class="col-xs-4">
        <button class="btn btn-filter rounded btn-block btn-all">TẤT CẢ</button>
      </div>
      <div class="col-xs-4">
        <button class="btn btn-filter rounded btn-block">NHÀ HÀNG</button>
      </div>
      <div class="col-xs-4">
        <button class="btn btn-filter rounded btn-block">ĐÓNG GÓI</button>
      </div>
    </div>

    <img src="../img/m-qc-1.png" alt="" class="img-responsive center-block">
    
    <div class="product-list">
      <div class="row row-small-space">
        <?php for($i = 0; $i < 4; $i++) { ?>
        <div class="col-xs-6">
          <div class="product-item">
            <a href="detail.html" class="img-3x2" style="background-image: url('../img/m-product-detail.png')"></a>
            <h2><a href="">Buffet tối 60 món Việt Nam, Á, Âu, lẩu</a></h2>
            <p>83/35 Phạm Văn Bạch, P. 15, Quận Bình Thạnh, TP.HCM</p>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div class="text-center">
      <button class="btn rounded btn-view-more">
        <span class="button-text">Xem thêm</span>
        <span class="button-dot"></span>
        <span class="button-dot"></span>
        <span class="button-dot"></span>
      </button>
    </div>
  </section>

  <section class="most-order most-place">
    <h2 class="section-header">Địa điểm phổ biến</h2>

    <div class="row row-small-space row-buttons">
      <div class="col-xs-4">
        <button class="btn btn-filter rounded btn-block btn-all">TẤT CẢ</button>
      </div>
      <div class="col-xs-4">
        <button class="btn btn-filter rounded btn-block">NHÀ HÀNG</button>
      </div>
      <div class="col-xs-4">
        <button class="btn btn-filter rounded btn-block">ĐÓNG GÓI</button>
      </div>
    </div>

    <img src="../img/m-qc-2.png" alt="" class="img-responsive center-block">
    
    <div class="product-list">
      <div class="row row-small-space">
        <?php for($i = 0; $i < 4; $i++) { ?>
        <div class="col-xs-6">
          <div class="product-item">
            <a href="detail.html" class="img-3x2" style="background-image: url('../img/m-product-detail.png')"></a>
            <h2><a href="">Buffet tối 60 món Việt Nam, Á, Âu, lẩu</a></h2>
            <p>83/35 Phạm Văn Bạch, P. 15, Quận Bình Thạnh, TP.HCM</p>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>

    <div class="text-center">
      <button class="btn rounded btn-view-more">
        <span class="button-text">Xem thêm</span>
        <span class="button-dot"></span>
        <span class="button-dot"></span>
        <span class="button-dot"></span>
      </button>
    </div>
  </section>

  <section class="step-2-become">
    <h2 class="section-header">3 bước để trở thành Thượng Đế !</h2>
    <div class="custom-row">
      <div class="icon"><i class="icon-marker"></i></div>
      <div class="info">
        <h3>1. Chọn địa điểm</h3>
        <p>Thực đơn đa dạng, hấp dẫn</p>
      </div>
    </div>
    
    <div class="custom-row">
      <div class="icon"><i class="icon-bag-2"></i></div>
      <div class="info">
        <h3>2. Chọn & đặt món</h3>
        <p>Chọn & đặt món yêu thích tiện lợi, dễ dàng</p>
      </div>
    </div>

    <div class="custom-row">
      <div class="icon"><i class="icon-check"></i></div>
      <div class="info">
        <h3>3. Nhận & thanh toán</h3>
        <p>Giao hàng trong ngày với hầu hết các món ăn</p>
      </div>
    </div>
  </section>

  <script>
    $(window).scroll(function(){
      var aTop = $('section.banner').height();

      if($(this).scrollTop() < 130){
        $('#seach-icon').hide();
      } else {
        $('#seach-icon').show();
      }
    });

    $(window).scroll();
  </script>
  
  <!-- Modal city selection -->
  <?php include 'modal/city-selection.php'; ?>

<?php include '_footer.php' ?>