<?php include '_header.php' ?>

<div class="page page-profile page-order-history">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      
      <?php $active = 'profile-order-history'; include '_profile-sidebar.php';?>

      <div class="page-content">
        <h1 class="page-title">Đơn đặt hàng</h1>

        <div class="content-wrapper">

          <div class="filter-wrapper">
            <form action="" class="form-inline">

              <div class="form-group">
                <label>Trạng thái</label>
                <select name="" class="form-control">
                  <option value="">Tất cả</option>
                  <option value="1">Đã giao hàng</option>
                </select>
              </div>

              <div class="form-group">
                <label>Từ ngày</label>
                <input type="text" class="form-control datepicker" value="01/07/2017">
              </div>

              <div class="form-group">
                <label>Đến ngày</label>
                <input type="text" class="form-control datepicker" value="30/07/2017">
              </div>
  
              <div class="form-group">
                <button type="submit" class="btn btn-normal">Tìm</button>
              </div>

            </form>
          </div>
          
          <div class="order-history-list">
            <table class="table">
              <thead>
                <tr>
                  <th width="1">STT</th>
                  <th width="1" class="nowrap">Mã đơn hàng</th>
                  <th>Ngày đặt</th>
                  <th>Nơi đặt</th>
                  <th width="150">Tổng tiền</th>
                  <th>Trạng thái</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i = 0; $i < 3; $i++) { ?>
                <tr>
                  <td><?=$i+1?></td>
                  <td><b>3454657</b></td>
                  <td>
                    <p>Đặt món: 22/10/2017 10:00</p>
                    <p>Giao hàng: 22/10/2017 10:00</p>
                  </td>
                  <td>
                    <div class="text-wrapper">
                      <p><b>Cửa hàng bánh Gia Phúc</b></p>
                      <p>360 Lý Thái Tổ, P. 1, Quận 3, TP. HCM</p>
                    </div>
                    <div class="action-wrapper">
                      <a href="" class="link">Đặt hàng lại</a>
                    </div>
                  </td>
                  <td>
                    <div class="text-wrapper">
                      <p><b class="red">219.000 VND</b></p>
                      <p>13 món (Giao hàng nhận tiền)</p>
                    </div>
                    <div class="action-wrapper">
                      <a href="" class="link">In hóa đơn</a>
                    </div>
                  </td>
                  <td>
                    <div class="text-wrapper">
                      <p>Đã giao hàng</p>
                    </div>
                    <div class="action-wrapper">
                      <a href="profile-order-detail.html" class="link">Xem chi tiết</a>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_footer.php' ?>