<?php if(!isset($active)) $active = 'profile'; ?>
<div class="sidebar">
  <ul class="nav nav-tabs" role="tablist">
    <li class="<?=$active == 'profile' ? 'active' : ''?>"><a href="profile.html">Thông tin cá nhân</a></li>
    <li class="<?=$active == 'profile-order-history' ? 'active' : ''?>"><a href="profile-order-history.html">Đơn đặt hàng</a></li>
    <li class="<?=$active == 'profile-payment-method' ? 'active' : ''?>"><a href="profile-payment-method.html">Phương thức thanh toán</a></li>
    <li class="<?=$active == 'profile-address' ? 'active' : ''?>"><a href="profile-address.html">Sổ địa chỉ</a></li>
  </ul>
</div>