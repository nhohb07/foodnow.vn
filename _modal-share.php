<div class="modal fade share-modal" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
          <h4>Chia sẻ cho nhóm</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-3">
              <img src="img/share-qr-code.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-xs-9">
              <p class="share-help">Copy link và gửi cho nhóm</p>
              <input type="text" readonly class="share-link" value="https://www.deliverynow.vn/ho-chi-minh/chicken-go-saigon-shop-o" onclick="copyShareLink(this)">
              <div class="share-help share-action">
                <span>Chia sẻ qua:</span>
                <a href="" target="_blank"><img src="img/icon-facebook-circle.png" alt=""></a>
                <a href="" target="_blank"><img src="img/icon-email-circle.png" alt=""></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
