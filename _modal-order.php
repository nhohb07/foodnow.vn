<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyAwTlz1kBY5NKHVJaMZ2jnqqy0-PMhA0P4"></script>

<div class="modal fade modal-header-transparent order-modal" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">

          <form autocomplete="off">

            <div id="carousel-order-steps" class="carousel slide " data-ride="carousel" data-interval="false">
              <div class="step-container">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-order-steps" class="step-nav active">
                    <span class="step-wrapper">
                      <span class="step-number">1</span>
                      <span class="step-dot">.</span>
                      <span class="step-text">THÔNG TIN</span>
                    </span>
                  </li>
                  <li data-target="#carousel-order-steps" class="step-nav">
                    <span class="step-wrapper">
                      <span class="step-number">2</span>
                      <span class="step-dot">.</span>
                      <span class="step-text">Đơn hàng</span>
                    </span>
                  </li>
                  <li data-target="#carousel-order-steps" class="step-nav">
                    <span class="step-wrapper">
                      <span class="step-number">3</span>
                      <span class="step-dot">.</span>
                      <span class="step-text">Hoàn tất</span>
                    </span>
                  </li>
                </ol>
              </div>

              <div class="carousel-inner" role="listbox">
                <div class="item step-1 active">
                  
                  <div class="step-1-wrapper">
                    <div id="map-canvas" class="map-container"></div>
                    <div class="step-1-info">
                      <div class="wrapper">
                        <div class="info-left">
                          <div class="store-info-section">
                            <div class="store-name">Chicken Go Saigon - Shop Online</div>
                            <div class="store-address">751/16 Trần Xuân Soạn, P. Tân Hưng, Quận 7, TP. HCM</div>
                          </div>
                          <div class="delivery-time-section">
                            <div class="header">Thời Gian Nhận Hàng</div>
                            <div class="delivery-input">
                              <input type="text" class="delivery-date pickadate" value="2017-09-14" readonly>
                              <select name="" class="delivery-time">
                                <option value="">Càng sớm càng tốt</option>
                                <option value="">Càng sớm càng tốt</option>
                              </select>
                            </div>
                          </div>
                          <div class="cost-section">
                            <div class="header">Vận chuyển và phí ước tính</div>
                            <div class="distance">Khoảng cách: 5,2 km  -  Thời gian: 15 phút</div>
                            <div class="cost-est">Phí: 50,000 đ</div>
                          </div>
                        </div>
                        <div class="info-right">
                          <div class="info-right-wrapper">
                            <!-- Form Add New Address, remove class hidden to show this -->
                            <div class="form-add-new-address hidden">
                              <div class="header">Địa Điểm Nhận Hàng</div>
                              <div data-toggle="validator" role="form" id="formAddNewAddress">

                                <div class="form-group display-table">
                                  <div class="form-inline row">
                                    <div class="form-group col-xs-7">
                                      <input type="text" class="form-control" placeholder="Họ tên" data-error="Hãy nhập họ tên của bạn" required>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-xs-5">
                                      <input type="number" class="form-control" placeholder="Số điện thoại" data-error="Hãy nhập số điện thoại" data-maxlength="11" required>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Địa chỉ" data-error="Hãy nhập địa chỉ của bạn" required>
                                  <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Ghi chú cho đơn hàng">
                                </div>

                                <div class="text-right">
                                  <button type="submit" class="btn btn-primary btn-submit">Tiếp tục</button>
                                </div>

                              </div>
                            </div>
                            <!-- END Form Add New Address -->
                            
                            <!-- Form Add New Address -->
                            <div class="form-select-address">
                              <div class="header">Địa Điểm Nhận Hàng</div>
                              <div class="list-address">
                                <ul>
                                  <li>
                                    <label class="checkbox-wrapper radio-default">
                                      <input type="radio" id="address-0" name="address" checked>
                                      <span></span>
                                    </label>
                                    <div class="address-info">
                                      <label for="address-0">
                                        <span class="address-name">Nguyễn Minh Toàn</span>
                                        <span class="address-phone">
                                          81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                                          0969740960
                                        </span>
                                      </label>
                                      <div class="address-action">
                                        <a onclick="toggleUpdateAddress(true)">Chỉnh sửa</a>
                                      </div>
                                    </div>
                                  </li>
                                  <li>
                                    <label class="checkbox-wrapper radio-default">
                                      <input type="radio" id="address-1" name="address">
                                      <span></span>
                                    </label>
                                    <div class="address-info">
                                      <label for="address-1">
                                        <span class="address-name">Nguyễn Minh Toàn</span>
                                        <span class="address-phone">
                                          81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                                          0969740960
                                        </span>
                                      </label>
                                      <div class="address-action">
                                        <a onclick="toggleUpdateAddress(true)">Chỉnh sửa</a>
                                      </div>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                              <div class="form-action">
                                <a onclick="toggleUpdateAddress(false)">Thêm điạ chỉ mới</a>
                              </div>

                              <div class="text-right">
                                <button type="button" onclick="gotoStep(2)" class="btn btn-primary btn-submit">Tiếp tục</button>
                              </div>
                            </div>
                            <!-- END Form Add New Address -->

                            <!-- Form Update Address, remove class hidden to show this -->
                            <div class="form-update-address hidden">
                              <div class="header">
                                <span class="is-update">Chỉnh sửa</span>
                                <span class="is-add-new">Thêm mới</span>
                                địa chỉ giao hàng
                              </div>
                              <div data-toggle="validator" role="form" id="formUpdateAddress">

                                <div class="form-group display-table">
                                  <div class="form-inline row">
                                    <div class="form-group col-xs-7">
                                      <input type="text" value="Nguyễn Văn A" class="form-control" placeholder="Họ tên" data-error="Hãy nhập họ tên của bạn" required>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-xs-5">
                                      <input type="number" value="0922134567" class="form-control" placeholder="Số điện thoại" data-error="Hãy nhập số điện thoại" data-maxlength="11" required>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Vui lòng điền CHÍNH XÁC 'tầng, số nhà, đường'" data-error="Hãy nhập địa chỉ của bạn" required>
                                  <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group display-table">
                                  <div class="form-inline row">
                                    <div class="form-group col-xs-4">
                                      <select class="form-control" required>
                                        <option value="" hidden disabled selected>Phường, xã</option>
                                        <option value="United States">United States</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                      </select>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-xs-4">
                                      <select class="form-control" required>
                                        <option value="" hidden disabled selected>Quận/huyện</option>
                                        <option value="United States">United States</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                      </select>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-xs-4">
                                    <select class="form-control" required>
                                        <option value="" hidden disabled selected>Tỉnh/Thành phố</option>
                                        <option value="United States">United States</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                      </select>
                                      <div class="help-block with-errors"></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="text-right">
                                  <button type="button" class="btn btn-default btn-normal" onclick="toggleUpdateAddress()">hủy</button>
                                  <button type="submit" onclick="return gotoStep(2, this)" class="btn btn-primary btn-submit">lưu lại</button>
                                </div>

                              </div>
                            </div>
                            <!-- END Form Update Address -->

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="item step-2">
                  <div class="step-2-wrapper">
                    <div class="info-left">

                      <div class="delivery-address-section">
                        <div class="header">Địa chỉ giao hàng</div>
                        <div class="address-detail">
                          <span class="address-name">Nguyễn Minh Toàn</span>
                          <span class="address-phone">
                            81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                            0969740960
                          </span>
                        </div>
                      </div>

                      <div class="payment-method-section">
                        <div class="header">Phương thức thanh toán</div>
                        <ul class="payment-method-list">
                          <li>
                            <label>
                              <input type="radio" name="payment_method" checked>
                              <span>Tiền mặt khi nhận hàng</span>
                            </label>
                          </li>
                          <li>
                            <label>
                              <input type="radio" name="payment_method">
                              <span>Internet Banking</span>
                            </label>
                          </li>
                          <li>
                            <label>
                              <input type="radio" name="payment_method">
                              <span>Creadit Visa / MASTER / JCB</span>
                            </label>
                          </li>
                        </ul>
                      </div>

                    </div>
                    <div class="info-right">

                      <div class="header">Chi tiết đơn hàng</div>

                      <div class="section section-scroll">
                        <table class="table">
                          <tr>
                            <td colspan="2">
                              <img src="img/icon-user.png" alt="">
                              <span class="user-name">Toàn Nguyễn</span>
                            </td>
                            <td class="text-right">3 món</td>
                          </tr>
                          <tr>
                            <td width="1" class="pr">1</td>
                            <td>Gà ta bó xôi chà bông</td>
                            <td class="text-right"><b>30,000 đ</b></td>
                          </tr>
                          <tr>
                            <td width="1" class="pr">1</td>
                            <td>Gà ta bó xôi chà bông</td>
                            <td class="text-right"><b>30,000 đ</b></td>
                          </tr>
                          <tr>
                            <td width="1" class="pr">1</td>
                            <td>Gà ta bó xôi chà bông</td>
                            <td class="text-right"><b>30,000 đ</b></td>
                          </tr>
                        </table>
                        <table class="table">
                          <tr>
                            <td colspan="2">
                              <img src="img/icon-user.png" alt="">
                              <span class="user-name">Lan Trần</span>
                            </td>
                            <td class="text-right">2 món</td>
                          </tr>
                          <tr>
                            <td width="1" class="pr">1</td>
                            <td>Gà ta bó xôi chà bông</td>
                            <td class="text-right"><b>30,000 đ</b></td>
                          </tr>
                          <tr>
                            <td width="1" class="pr">1</td>
                            <td>Gà ta bó xôi chà bông</td>
                            <td class="text-right"><b>30,000 đ</b></td>
                          </tr>
                        </table>
                      </div>

                      <div class="separator"></div>

                      <div class="section">
                        <table class="table">
                          <tr>
                            <td>Cộng</td>
                            <td class="text-right"><b>120,000 đ</b></td>
                          </tr>
                          <tr>
                            <td>Phí vận chuyển (Est.)</td>
                            <td class="text-right"><span class="red">5,000 đ/km</span></td>
                          </tr>
                          <tr>
                            <td colspan="2" class="coupon-code">
                              <span>Nhập mã khuyến mãi</span>
                              <input type="text" placeholder="NHẬP MÃ" class="is-empty" onkeyup="checkCouponInputValue(this)">
                            </td>
                          </tr>
                          <tr>
                            <td>Tạm tính</td>
                            <td class="text-right"><b class="red">125,000 đ</b></td>
                          </tr>
                        </table>
                      </div>

                      <div class="section text-right step-2-actions">
                        <button type="button" class="btn btn-default btn-normal" onclick="gotoStep(1);">QUAY LẠI</button>
                        <button type="submit" class="btn btn-primary btn-submit" onclick="gotoStep(3);">TIẾP TỤC</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item step-3">
                  <div class="step-3-wrapper">
                    <h2>XIN CHÚC MỪNG!</h2>
                    <div class="desc">
                      Các nguyên liệu đã được sẳn sàng để chế biến, Thượng Đế hãy chờ trong chốt lát ạ!
                    </div>
                    <div class="step-3-actions">
                      <a href="#" class="btn btn-primary btn-normal">Theo dõi đơn hàng</a>
                      <a href="/" class="btn btn-primary btn-submit">Về trang chủ</a>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('#orderModal').on('show.bs.modal', function () {
      $('.carousel').carousel(0);
      $('.step-nav').css('color', '#fff');
    });

    $('#orderModal').on('shown.bs.modal',function(){
      initialize();
    });

    //validate form
    $('#formAddNewAddress, #formUpdateAddress').validator();

    //Pick A Date
    $(".pickadate").pickadate({
      format: "yyyy-mm-dd",
      firstDay: 1,
      min: new Date(),
      closeOnSelect: true,
      onOpen: function() {
        $('.step-container').addClass('pickadate-opened');
      },
      onClose: function() {
        $('.step-container').removeClass('pickadate-opened');
      },
    });

    //chosen selectbox, can load with ajax and init chosen again with below function
    // $('.chosen-select').chosen({
    //   disable_search: false
    // });

    // //trigger
    // $('.chosen-select').on('chosen:showing_dropdown', function (evt, params) {
    //   console.log('show')
    //   $('#carousel-order-steps > .carousel-inner').addClass('no-overflow');
    // });
    // $('.chosen-select').on('chosen:hiding_dropdown', function (evt, params) {
    //   console.log('hide')
    //   $('#carousel-order-steps > .carousel-inner').removeClass('no-overflow');
    // });

    //grayout prev step
    grayPrevStep();
  });

  function grayPrevStep() {
    $('.step-nav.active').prevAll().css('color', 'rgba(255, 255, 255, 0.5)');
  }

  function initialize() {
    var map_canvas = document.getElementById('map-canvas');
    var map_options = {
      center: new google.maps.LatLng(10.762622, 106.660172),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options)
  }
  google.maps.event.addDomListener(window, 'load', initialize);

  //toggle form update address
  function toggleUpdateAddress(isUpdate) {
    var formSelectAddress = $('.form-select-address');
    var formUpdateAddress = $('.form-update-address');

    if(formSelectAddress.hasClass('hidden')) {
      //hiden form and show select list
      formSelectAddress.removeClass('hidden');
      formUpdateAddress.addClass('hidden').removeClass('is-update');
    } else {
      //show form and hide select list
      formUpdateAddress.removeClass('hidden');
      formSelectAddress.addClass('hidden');

      if(isUpdate) {
        formUpdateAddress.addClass('is-update');
      }
    }
  }

  //go to special step
  function gotoStep(step, element) {
    if($(element).hasClass('disabled')) {
      return false;
    }

    $('#carousel-order-steps').carousel(step - 1);
    grayPrevStep();
    return false;
  }

  //check value for coupon input and show/hide border on input
  function checkCouponInputValue(element) {
    if($(element).val() != '') {
      $(element).removeClass('is-empty');
    } else {
      $(element).addClass('is-empty');
    }
  }
</script>