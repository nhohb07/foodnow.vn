<div class="modal fade modal-add-cart" id="modalAddCart" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Tùy chọn món'; include 'modal-header.php'; ?>

      <div class="modal-body no-padding-vertical">

        <div class="product-info custom-row">
          <div class="pull-left">
            <img src="../img/m-product-detail.png" alt="" class="img-responsive" width="90" id="product-image">
          </div>
          <div class="column-right">
            <h5>Chicken Go Saigon</h5>
            <small>Nguyên con</small>
            <div class="price">30,000 đ</div>
          </div>
        </div>

        <div class="group-separator">
          Thêm <span>(Chọn tối đa 1 món)</span>
        </div>

        <ul class="options">
          <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
          <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
          <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
          <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
          <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
          <li><label><input type="checkbox"><span>Trứng non <b>+50,000 đ</b></span></label></li>
        </ul>

      </div>

      <div class="modal-footer">
        <div class="product-quantity">
          <span class="quantity-button quantity-decrease"><i class="icon-minus"></i></span>
          <span class="quantity-text">1</span>
          <span class="quantity-button quantity-increase"><i class="icon-plus"></i></span>
        </div>
                  
        <button 
          class="btn btn-small btn-green rounded btn-block" 
          onclick="flyToElement('#product-image', '#cartFlyHere');$('#modalAddCart').modal('hide')"
        >ĐỒNG Ý +359,000 Đ</button>
      </div>
    </div>
  </div>
</div>