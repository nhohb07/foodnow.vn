<div class="modal-header">
  <h4 class="modal-title">
    <?php if(!isset($modalHeader)) { ?>
    <a href="">
      <img src="../img/logo.png" alt="" class="logo">
    </a>
    <?php } else {
      echo $modalHeader;
    } ?>
  </h4>

  <button data-dismiss="modal" class="btn-close"><i class="icon-close"></i></button>
</div>
