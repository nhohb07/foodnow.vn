$(function(){
  initProductImageSwitcher();
  initOwl();

  $(".datepicker").datepicker({
    dateFormat: "dd/mm/yy"
  });
});

// Add smooth scrolling on all links inside the navbar
$("#menu-tab-switcher a").on('click', function(event) {
  event.preventDefault();
  var hash = this.hash;

  $('html, body').animate({
    scrollTop: $(hash).offset().top - 100
  }, 800);
});

function copyToClipboard(text, element) {
  if (window.clipboardData && window.clipboardData.setData) {
    // IE specific code path to prevent textarea being shown while dialog is visible.
    if(clipboardData.setData("Text", text)) {// Security exception may be thrown by some browsers.
  		$(element).text('Copied');
  		setTimeout(function() {$(element).text('Copy code');}, 1000);
  		
  		return true;
  	}

    return false;
  } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
    var textarea = document.createElement("textarea");
    textarea.textContent = text;
    textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
    document.body.appendChild(textarea);
    textarea.select();
    try {
    	if(document.execCommand("copy")) {// Security exception may be thrown by some browsers.
    		$(element).text('Copied');
    		setTimeout(function() {$(element).text('Copy code');}, 1000);

    		return true;
    	}

      return false;
    } catch (ex) {
      console.warn("Copy to clipboard failed.", ex);
      return false;
    } finally {
      document.body.removeChild(textarea);
    }
  }
}

function flyToElement(flyer, flyingTo) {
  var $func = $(this);
  var divider = 3;
  var flyerClone = $(flyer).clone();
  $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 19999});
  $('body').append($(flyerClone));
  var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
  var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;
   
  $(flyerClone).animate({
    opacity: 0.4,
    left: gotoX,
    top: gotoY,
    width: $(flyer).width()/divider,
    height: $(flyer).height()/divider,
  }, 700,
  function () {
    $(flyingTo).fadeOut('fast', function () {
      $(flyingTo).fadeIn('fast', function () {
        $(flyerClone).fadeOut('fast', function () {
          $(flyerClone).remove();
        });
      });
    });
  });
}

function initProductImageSwitcher() {
  //product image switcher
  $('.carousel-fade').carousel({
    interval: false
  });
  var productImageSwitcher;
  $('.carousel-fade').hover(function () {
    var carousel = $(this);
    productImageSwitcher = setInterval(function () {
      carousel.carousel('next');
    }, 1000);
  }, function() {
    clearInterval(productImageSwitcher);
    $(this).carousel(0);
  });
}

function initOwl(time) {
  var items = {
    morning: [1,2,3,4],
    afternoon: [2,3,4,1],
    evening: [3,4,1,2],
    package: [4,3,2,1],
  };

  var itemTime = items[time] || items['morning'];

  var html = '';

  for(var i = 0; i < 8; i++) {
    html += '<a href="#">';
      html += '<span class="product-ribon">20 %</span>';
      html += '<div class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">';
        html += '<div class="carousel-inner">';

        for(var j = 0; j < itemTime.length; j++) {
          html += '<div class="item '+(j == 0 ? 'active' : '')+'">';
            html += '<img src="img/product-featured-'+(itemTime[j])+'.png" class="img-responsive center-block">';
          html += '</div>';
        }

        html += '</div>';
      html += '</div>';

      html += '<h2>Buffet tối 60 món Việt Nam, Á, Âu, lẩu tại Buffet</h2>';
      html += '<div class="product-address">';
        html += '83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, Tp.HCM';
      html += '</div>';
    html += '</a>';
  }

  // replace old elements
  $('#home-owl-slider').html(html);

  // destroy current
  $('#home-owl-slider').owlCarousel('destroy'); 
  // reinit slider
  $("#home-owl-slider").owlCarousel({
    items: 4,
    nav:true,
    navText: ['<span class="icon-arrow-prev"></span>', '<span class="icon-arrow-next"></span>'],
    margin: 34,
    loop: true
  });
  
  initProductImageSwitcher();
}