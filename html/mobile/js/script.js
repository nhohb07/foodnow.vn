$(function(){
  initHomeOwlSlider();

  $("#detail-image-slider").owlCarousel({
    items: 1,
    nav:true,
    margin: 0,
    loop: true,
  });

  $(".datepicker").datepicker({
    dateFormat: "dd/mm/yy"
  });
});

function modalOpenOtherModal(closeModal, openModal) {
  $(closeModal).modal('hide');
  $(openModal).modal('show');

  $(openModal).on('shown.bs.modal', function () {
    $('body').addClass('modal-open');
  })

  return false;
}

function initHomeOwlSlider() {
  var html = '';

  for(var i = 0; i < 8; i++) {
    html += '<a href="#">';
      html += '<span class="product-ribon">20 %</span>';
      html += '<img src="../img/m-product-slide-'+(Math.floor(Math.random() * 2) + 1)+'.png" class="img-responsive center-block">';

      html += '<h2>Buffet tối 60 món Việt Nam, Á, Âu, lẩu tại Buffet</h2>';
      html += '<div class="product-address">';
        html += '83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, Tp.HCM';
      html += '</div>';
    html += '</a>';
  }

  // replace old elements
  $('#home-product-slider').html(html);

  // destroy current
  $('#home-product-slider').owlCarousel('destroy'); 
  // reinit slider
  $("#home-product-slider").owlCarousel({
    items: 2,
    nav:false,
    margin: 10,
    loop: true,
    center: false
  });
}

function copyToClipboard(text, element) {
  if (window.clipboardData && window.clipboardData.setData) {
    // IE specific code path to prevent textarea being shown while dialog is visible.
    if(clipboardData.setData("Text", text)) {// Security exception may be thrown by some browsers.
      $(element).text('Copied');
      setTimeout(function() {$(element).text('Copy code');}, 1000);
      
      return true;
    }

    return false;
  } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
    var textarea = document.createElement("textarea");
    textarea.textContent = text;
    textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
    document.body.appendChild(textarea);
    textarea.select();
    try {
      if(document.execCommand("copy")) {// Security exception may be thrown by some browsers.
        $(element).text('Copied');
        setTimeout(function() {$(element).text('Copy code');}, 1000);

        return true;
      }

      return false;
    } catch (ex) {
      console.warn("Copy to clipboard failed.", ex);
      return false;
    } finally {
      document.body.removeChild(textarea);
    }
  }
}

function flyToElement(flyer, flyingTo) {
  var $func = $(this);
  var divider = 3;
  var flyerClone = $(flyer).clone();
  $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 19999});
  $('body').append($(flyerClone));
  var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
  var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;
   
  $(flyerClone).animate({
    opacity: 0.4,
    left: gotoX,
    top: gotoY,
    width: $(flyer).width()/divider,
    height: $(flyer).height()/divider,
  }, 700,
  function () {
    $(flyingTo).fadeOut('fast', function () {
      $(flyingTo).fadeIn('fast', function () {
        $(flyerClone).fadeOut('fast', function () {
          $(flyerClone).remove();
        });
      });
    });
  });
}