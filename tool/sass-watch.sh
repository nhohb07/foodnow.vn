#!/bin/bash

while inotifywait -e modify $1scss/*; 
do php tool/sass-builder.php $1 $2; 
done