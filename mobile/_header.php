<?php $fileName = basename($_SERVER['PHP_SELF'], '.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>www.foodnow.vn - mobile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../lib/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="../lib/OwlCarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="../lib/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/<?=$fileName?>.css">

    <script src="../lib/jquery.min.js"></script>
    <script src="../lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyAwTlz1kBY5NKHVJaMZ2jnqqy0-PMhA0P4"></script>
  </head>
  <body <?=isset($bodyAttrs) ? $bodyAttrs : null?>>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1261715420604681";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    <header>

      <div class="row row-small-space">

        <div class="col-xs-4">
          <a href="">
            <img src="../img/logo.png" alt="" class="logo">
          </a>
        </div>

        <div class="col-xs-8 text-right">

          <div class="pull-right">
            <a class="search-icon" id="seach-icon" style="<?=($fileName == 'index') ? 'display: none' : ''?>" data-toggle="modal" data-target="#modalSearch">
              <i class="icon-search-light"></i>
            </a>

            <!-- Add/Remove class hidden -->
            <div class="logged">
              <a class="cart-header" data-toggle="modal" data-target="#modalCart" id="cartFlyHere">
                <i class="icon-bag"></i>
                <span class="cart-header-badge">3</span>
              </a>

              <div class="btn-group-profile">
                <button type="button" class="dropdown-toggle city-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="../img/m-icon-user.png" alt="">
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li>
                    <a data-toggle="modal" data-target="#modalOrderList">
                      <i class="icon-order-history"></i>
                      Lịch sử đặt món
                    </a>
                  </li>
                  <li>
                    <a data-toggle="modal" data-target="#modalProfile">
                      <i class="icon-setting"></i>
                      Cập nhật tài khoản
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="icon-logout"></i>
                      Thoát
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <!-- Add/Remove class hidden -->
            <div class="not-logged hidden">
              <button class="button-login" data-toggle="modal" data-target="#modalLogin">ĐĂNG NHẬP</button>
            </div>
          </div>

        </div>

      </div>

    </header>

    <main>      
  