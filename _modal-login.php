<div class="modal fade modal-header-transparent login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">
          <div id="login-modal-carousel" class="carousel slide carousel-fade-effect" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
              
              <!-- FORM LOGIN & REGISTER -->
              <div class="item active">
                <div class="body-wrapper form-login-register">
                  <div class="lm-left">
                    <h4>Đăng nhập</h4>
                    <p>Đăng nhập để theo dõi đơn hàng, lưu danh sách sản phẩm yêu thích, nhận nhiều ưu đãi hấp dẫn.</p>
                  </div>
                  <div class="lm-right">
                    <div>
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="border active">
                          <a href="#login" aria-controls="login" role="tab" data-toggle="tab">Đăng nhập</a>
                        </li>
                        <li role="presentation" class="">
                          <a href="#register" aria-controls="register" role="tab" data-toggle="tab">Tạo tài khoản</a>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">

                        <!-- Form Login -->
                        <div role="tabpanel" class="tab-pane fade in active" id="login">
                          <form>
                            <input type="email" name="username" class="form-control" placeholder="Email / Số điện thoại / Tên đăng nhập">
                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                            <p class="text-right">
                              <a data-target="#login-modal-carousel" data-slide-to="1" class="link-default link-forgot-password">Quên mật khẩu</a>
                            </p>
                            <button type="submit" class="button-login btn-block">ĐĂNG NHẬP</button>

                            <div class="form-footer">
                              <p class="help">Hoặc đăng nhập bằng</p>
                              <div class="row">
                                <div class="col-xs-6">
                                  <a class="btn btn-block button-fb">
                                    <i class="icon-png-facebook"></i>
                                    <span>FACEBOOK</span>
                                  </a>
                                </div>
                                <div class="col-xs-6">
                                  <a class="btn btn-block button-google">
                                    <i class="icon-png-google"></i>
                                    <span>GOOGLE</span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                        <!-- End Form Login -->

                        <!-- Form Register -->
                        <div role="tabpanel" class="tab-pane fade email-form" id="register">
                          <form>
                            <!-- Register with email -->
                            <input type="email" name="email" class="form-control input-email-form" placeholder="Email">
                            
                            <!-- Register with phone -->
                            <div class="form-control input-phone-form input-phone-number">
                              <input type="text" name="phone" placeholder="Số điện thoại">
                              <button><i class="icon-arrow-right-long"></i> Gửi mã xác minh</button>
                            </div>
                            <input type="text" name="pin-code" class="form-control input-phone-form" placeholder="Nhập mã xác minh từ số điện thoại trên">

                            <input type="text" name="username" class="form-control" placeholder="Tên đăng nhập">
                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                            <input type="password" name="confirm_password" class="form-control" placeholder="Xác nhận mật khẩu">
                            <div class="form-control">
                              <input type="text" name="captcha" placeholder="Nhập mã xác nhận">
                              <img src="img/icon-captcha.png" alt="" class="pull-right">
                            </div>
                            <button type="submit" class="button-login btn-block">ĐĂNG KÝ</button>

                            <div class="term">
                              Bằng việc đăng kí, bạn đã đồng ý với Foodnow về <br>
                              <a href="">Điều khoản dịch vụ & Chính sách bảo mật</a>
                            </div>

                            <div class="form-footer">
                              <p class="help">Hoặc đăng nhập bằng</p>
                              <div class="row">
                                <div class="col-xs-6 input-email-form">
                                  <a class="btn btn-block button-google" onclick="$('#register').removeClass('email-form')">
                                    <i class="icon-png-phone"></i>
                                    <span>SỐ ĐIỆN THOẠI</span>
                                  </a>
                                </div>
                                <div class="col-xs-6 input-phone-form">
                                  <a class="btn btn-block button-google" onclick="$('#register').addClass('email-form')">
                                    <i class="icon-png-email"></i>
                                    <span>EMAIL</span>
                                  </a>
                                </div>
                                <div class="col-xs-6">
                                  <a class="btn btn-block button-fb">
                                    <i class="icon-png-facebook"></i>
                                    <span>FACEBOOK</span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                        <!-- End FOrm Register -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END FORM LOGIN & REGISTER -->
              
              <!-- FORM FORGOT PASSWORD -->
              <div class="item">
                <div class="body-wrapper form-forgot-password">
                  <div class="lm-left">
                    <h4>Quên mật khẩu</h4>
                    <p>Hãy nhập vào Email hoặc số điện thoại đã đăng ký trước đó để nhận lại mật khẩu mới.</p>
                  </div>
                  <div class="lm-right">
                    <h2 class="form-header">Quên mật khẩu</h2>
                    <div class="form-icon">
                      <img src="img/icon-forgot-password.png" alt="" class="img-responsive center-block">
                    </div>

                    <form action="" onsubmit="return submitFormForgotPassword()">
                      <input type="text" name="username" class="form-control" placeholder="Email / Số điện thoại">
                      <button type="submit" class="button-login btn-block">GỬI ĐI</button>
                    </form>
                  </div>
                </div>
              </div>
              <!-- END FORM FORGOT PASSWORD -->

              <!-- FORM ACTIVE PIN -->
              <div class="item">
                <div class="body-wrapper form-forgot-password">
                  <div class="lm-left">
                    <h4>Mã xác nhận</h4>
                    <p>Hãy nhập vào Mã xác nhận trong email hoặc số điện thoại đã đăng ký trước đó để nhận lại mật khẩu mới.</p>
                  </div>
                  <div class="lm-right">
                    <h2 class="form-header">Mã xác nhận</h2>
                    <div class="form-icon">
                      <img src="img/icon-active-pin.png" alt="" class="img-responsive center-block">
                    </div>

                    <form action="">
                      <input type="text" name="username" class="form-control" placeholder="Mã xác nhận">
                      <button type="submit" class="button-login btn-block">XÁC NHẬN</button>
                    </form>
                  </div>
                </div>
              </div>
              <!-- END FORM ACTIVE PIN -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#loginModal').on('show.bs.modal', function () {
    $('.carousel').carousel(0);
  });

  function submitFormForgotPassword() {
    $('.carousel').carousel(2);
    return false;
  }
</script>