<div class="modal fade modal-change-password" id="modalChangePassword" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'ĐỔI MẬT KHẨU'; include 'modal-header.php'; ?>

      <div class="modal-body">
        <form action="" data-toggle="validator">
          <div class="form-body">
            <!-- Old Password -->
            <div class="form-group">
              <label class="control-label">Mật khẩu cũ:</label>
              <input type="password" class="form-control" required data-error="Hãy nhập mật khẩu cũ">
              <div class="help-block with-errors"></div>
            </div>
              
            <!-- New Password -->
            <div class="form-group">
              <label class="control-label">Mật khẩu mới</label>
              <input type="password" class="form-control" id="password" data-minlength="6" data-error="Mật khẩu ít nhất 6 ký tự" required>
              <div class="help-block with-errors"></div>
            </div>
              
            <!-- Confirm Password -->
            <div class="form-group">
              <label class="control-label">Nhập lại mật khẩu</label>
              <input type="password" class="form-control" data-match="#password" data-error="Không giống với mật khẩu mới" required>
              <div class="help-block with-errors"></div>
            </div>
          </div>

          <div class="form-footer">
            <button class="btn btn-block btn-green btn-normal" type="submit">Lưu thay đổi</button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>