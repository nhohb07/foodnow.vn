<div class="modal fade modal-address-list" id="modalAddressList" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Sổ địa chỉ'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">

        <ul>
          <?php for($i = 0; $i < 2; $i++) { ?>
          <li class="address-item">
            <label class="radio-wrapper radio-default">
              <input type="radio" id="address-<?=$i?>" name="address" checked>
              <span></span>
            </label>
            <div class="address-item-info">
              <label for="address-<?=$i?>">Chọn địa chỉ giao hàng</label>
              <div class="address-item-detail">
                <div class="">
                  <div class="address-item-name">Nguyễn Minh Toàn</div>
                  <div class="address-item-phone">
                    81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                    0969740960
                  </div>
                </div>
                <div class="address-item-action">
                  <a class="link link-edit">Sửa</a>
                  <a class="link link-remove">Xóa</a>
                </div>
              </div>
            </div>
          </li>
          <?php } ?>

        </ul>

        <div class="actions">
          <a class="link link-edit" data-toggle="modal" data-target="#modalAddAddress">Thêm địa chỉ mới</a>
        </div>

      </div>
    </div>
  </div>
</div>