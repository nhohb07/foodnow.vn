#!/bin/bash
cd ..
rm -rf html/*
cp -rf css/ font/ img/ js/ lib/ scss/ tool/ html

mkdir html/mobile
cp -rf mobile/css/ mobile/js/ mobile/scss html/mobile

function php2html() {
  exportDir='html'

  if [[ $1 =~ 'mobile/' ]]
    then
      exportDir='../html/mobile'
      cd mobile
  fi

  pattern='^[a-z\-]+\.php$'
  for file in *.php; do
    if [[ $file =~ $pattern ]] 
    then
      fileName=$(basename "$file")
      extension="${fileName##*.}"
      fileName="${fileName%.*}"

      commandStr="php $fileName.php >> ${exportDir}/$fileName.html"
      eval "$commandStr"
      echo "$commandStr"
    fi
  done
}

php2html
php2html mobile/