<?php 
  $bodyAttrs = 'data-spy="scroll" data-target="#menu-tab-switcher" data-offset="100"';
  include '_header.php';

  $menuLeftList = [
    'TRÀ SỮA','TRÀ NGUYÊN CHẤT','COFFEE','TRÀ SỮA ĐẶC BIỆT','SODA Ý','ĐÁ XAY','SỮA CHUA'
  ];
?>
  <section class="image-slider">
    <div class="owl-carousel" id="detail-image-slider">
      <img src="../img/m-product-detail.png" alt="" class="img-responsive center-block">
      <img src="../img/m-product-detail.png" alt="" class="img-responsive center-block">
    </div>
  </section>

  <section class="store-info">
    <h1>Whisk - Bánh Cheese Tart - Bitexco Tower</h1>
    <p>Lầu 8, Tòa nhà Jabes 1, 244 Cống Quỳnh, P. Phạm Ngũ Lão, Quận 1, Tp.HCM</p>
  </section>

  <section class="review-working-time">
    <div class="custom-row">
      <div class="review">
        <div class="wrapper">
          <span class="store-review-star">3,5</span>
          <i class="icon-star"></i>
          <p>15 bình luận</p>
        </div>
      </div>

      <div class="working-time">
        <div class="wrapper">
          <ul>
            <li>
              <span class="icon"><i class="icon-time"></i></span>
              <span class="text">09:30 - 21:00</span>
            </li>
            <li>
              <span class="icon"><i class="icon-money-circle"></i></span>
              <span class="text">39,000 - 220,000</span>
            </li>
            <li>
              <span class="icon"><i class="icon-status active"></i></span>
              <span class="text"><b>Đang mở cửa</b></span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="payment-shipping-info">
    <div class="custom-row">
      <div class="column-payment">
        <h3>THANH TOÁN</h3>
        <div>
          <img src="../img/payment-jcb.png" alt="">
          <img src="../img/visa.png" alt="">
          <img src="../img/payment-other.png" alt="">
        </div>
      </div>

      <div class="column-separator">&nbsp;</div>

      <div class="column-shipping">
        <h3>PHÍ VẬN CHUYỂN</h3>
        <div>7,000 đ/km</div>
      </div>

      <div class="column-separator">&nbsp;</div>

      <div class="column-shipper">
        <h3>GIAO HÀNG</h3>
        <div>
          <img src="../img/shipping-giaohangnhanh.png" alt="" class="img-responsive img-shipper">
        </div>
      </div>
    </div>
  </section>

  <section class="coupon-code">
    <span class="red">Giảm 50%</span> - Nhập mã:  <span class="red">CHICKGO50</span>
  </section>

  <hr>

  <section class="menu">
    <ul class="nav nav-tabs hidden" role="tablist">
      <?php foreach($menuLeftList as $key => $menu) { ?>
      <li class="<?=$key == 0 ? 'active' : ''?>"><a href="#<?=$key?>"><?=$menu?></a></li>
      <?php } ?>
    </ul>

    <?php foreach($menuLeftList as $key => $menu) { ?>
      <h4 class="menu-content-heading" id="<?=$key?>"><?=$menu?></h4>
      <div class="menu-content-list">
        <?php for($i = 0; $i < 4; $i++) { ?>
        <div class="product-item">
          <div class="product-img">
            <a href="../img/product-detail-3.jpg" class="menu-images" title="">
              <img src="../img/product-3.png" id="product-image-<?=$key.$i?>" alt="" class="img-responsive product-image">
            </a>
          </div>

          <div class="product-info">
            <h2 class="product-name">Chicken Go Saigon</h2>
            <div class="product-desc">Nguyên con</div>
            <div class="product-price">30,000 đ</div>
          </div>

          <div class="product-actions">
            <a class="btn-add-cart" data-toggle="modal" data-target="#modalAddCart"><i class="icon-plus"></i></a>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
  </section>

  <section class="cart">
    <div class="custom-row">
      <div class="total" data-toggle="modal" data-target="#modalCartGroup">
        <div class="total-text">2 phần - 1 người</div>
        <div class="total-price">230,000 đ</div>
      </div>

      <div class="group">
        <button class="btn rounded" data-toggle="modal" data-target="#modalShare">Nhóm cùng đặt</button>
      </div>

      <div class="actions">
        <button>Xóa</button>
      </div>
    </div>

    <div class="buttons">
      <!-- Add/Remove attr disabled -->
      <button class="btn rounded btn-block" data-toggle="modal" data-target="#modalPay">đặt trước</button>
    </div>
  </section>

<?php include '_footer.php' ?>