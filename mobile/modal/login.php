<div class="modal fade modal-login" id="modalLogin" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <div class="modal-title">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#login" aria-controls="login" role="tab" data-toggle="tab">ĐĂNG NHẬP</a></li>
            <li role="presentation"><a href="#register" aria-controls="register" role="tab" data-toggle="tab">TẠO TÀI KHOẢN</a></li>
          </ul>
        </div>

        <button data-dismiss="modal" class="btn-close"><i class="icon-close"></i></button>
      </div>

      <div class="modal-body">
        <div class="tab-content">

          <!-- Form Login -->
          <div role="tabpanel" class="tab-pane fade in active" id="login">
            <form action="" data-toggle="validator">
              <div class="form-body">
                <div class="form-group">
                  <input type="text" name="username" class="form-control rounded shadow" placeholder="Email / Số điện thoại" required data-error="Hãy nhập email hoặc số điện thoại">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <input type="password" name="password" class="form-control rounded shadow" placeholder="Mật khẩu" required data-error="Hãy nhập mật khẩu">
                  <div class="help-block with-errors"></div>
                </div>

                <p class="text-right">
                  <a onclick="modalOpenOtherModal('#modalLogin', '#modalForgotPassword')" class="link-default link-forgot-password">Quên mật khẩu</a>
                </p>
                <button type="submit" class="btn-green btn-block">ĐĂNG NHẬP</button>
              </div>

              <div class="form-footer">
                <p class="help">Hoặc đăng nhập bằng</p>
                <div class="row row-small-space">
                  <div class="col-xs-6">
                    <a class="btn btn-block button-fb">
                      <i class="icon-png-facebook"></i>
                      <span>FACEBOOK</span>
                    </a>
                  </div>
                  <div class="col-xs-6">
                    <a class="btn btn-block button-google">
                      <i class="icon-png-google"></i>
                      <span>GOOGLE</span>
                    </a>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!-- End Form Login --> 

          <!-- Form Register -->
          <div role="tabpanel" class="tab-pane fade email-form" id="register">
            <form action="" data-toggle="validator">
              <!-- Register with email -->
              <div class="form-group input-email-form">
                <input type="email" name="email" id="input-email" class="form-control rounded shadow" placeholder="Email" required data-error="Hãy nhập email của bạn">
                <div class="help-block with-errors"></div>
              </div>
              
              <!-- Register with phone -->
              <div class="form-group input-phone-form">
                <input type="text" name="phone" id="input-phone" class="form-control rounded shadow" placeholder="Số điện thoại" data-error="Hãy nhập số điện thoại">
                <div class="help-block with-errors"></div>
              </div>
              <button type="button" class="btn btn-block no-shadow btn-send-pincode input-phone-form"><i class="icon-arrow-right-long"></i> Gửi mã xác minh</button>

              <div class="form-group input-phone-form">
                <input type="text" name="pin-code" id="input-pincode" class="form-control rounded shadow" placeholder="Nhập mã xác minh từ số điện thoại trên" data-error="Hãy nhập mã xác minh từ số điện thoại trên">
                <div class="help-block with-errors"></div>
              </div>

              <div class="form-group">
                <input type="text" name="username" class="form-control rounded shadow" placeholder="Tên đăng nhập" required data-error="Hãy nhập tên đăng nhập">
                <div class="help-block with-errors"></div>
              </div>

              <div class="form-group">
                <input type="password" name="password" class="form-control rounded shadow" id="password" placeholder="Mật khẩu" required data-error="Mật khẩu ít nhất 6 ký tự">
                <div class="help-block with-errors"></div>
              </div>

              <div class="form-group">
                <input type="password" name="confirm_password" class="form-control rounded shadow" placeholder="Xác nhận mật khẩu"  data-match="#password" data-error="Không giống với mật khẩu" required>
                <div class="help-block with-errors"></div>
              </div>

              <div class="form-group">
                <div class="form-control rounded shadow">
                  <input type="text" name="captcha" placeholder="Nhập mã xác nhận" required data-error="Hãy nhập mã xác nhận">
                  <img src="../img/icon-captcha.png" alt="" class="pull-right">
                </div>
                <div class="help-block with-errors"></div>
              </div>

              <button type="submit" class="btn-green btn-block">ĐĂNG KÝ</button>

              <div class="term">
                Bằng việc đăng kí, bạn đã đồng ý với Foodnow về <br>
                <a href="">Điều khoản dịch vụ & Chính sách bảo mật</a>
              </div>

              <div class="form-footer">
                <p class="help">Hoặc đăng nhập bằng</p>
                <div class="row row-small-space">
                  <div class="col-xs-6 input-email-form">
                    <a class="btn btn-block button-google" onclick="switchRegisterTab('phone')">
                      <i class="icon-png-phone"></i>
                      <span>SĐT</span>
                    </a>
                  </div>
                  <div class="col-xs-6 input-phone-form">
                    <a class="btn btn-block button-google" onclick="switchRegisterTab('email')">
                      <i class="icon-png-email"></i>
                      <span>EMAIL</span>
                    </a>
                  </div>
                  <div class="col-xs-6">
                    <a class="btn btn-block button-fb">
                      <i class="icon-png-facebook"></i>
                      <span>FACEBOOK</span>
                    </a>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!-- End FOrm Register -->
        </div>
      </div>

    </div>
  </div>
</div>

<script>
  function switchRegisterTab(type) {
    if(type === 'email') {
      $('#register').addClass('email-form');
      $('#input-phone').removeAttr('required');
      $('#input-pincode').removeAttr('required');
      $('#input-email').attr('required', true);
    } else {
      $('#register').removeClass('email-form');
      $('#input-phone').attr('required', true);
      $('#input-pincode').attr('required', true);
      $('#input-email').removeAttr('required');
    }
  }
</script>