<div class="modal fade modal-forgot-password" id="modalEnterNewPassword" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'MẬT KHẨU MỚI'; include 'modal-header.php'; ?>

      <div class="modal-body">
        <div class="form-icon">
          <img src="../img/icon-active-pin.png" alt="" class="img-responsive center-block" width="70">
        </div>

        <div class="text-help">
          Hãy nhập vào mật khẩu mới để đăng nhập cho lần đặt món kế tiếp nhé!
        </div>

        <form action="" data-toggle="validator">
          <!-- New Password -->
          <div class="form-group">
            <input type="password" class="form-control rounded shadow" id="inputEnterNewPassword" data-minlength="6" data-error="Mật khẩu ít nhất 6 ký tự" required placeholder="Mật khẩu mới">
            <div class="help-block with-errors"></div>
          </div>
            
          <!-- Confirm Password -->
          <div class="form-group">
            <input type="password" class="form-control rounded shadow" data-match="#inputEnterNewPassword" data-error="Không giống với mật khẩu mới" required placeholder="Nhập lại mật khẩu mới">
            <div class="help-block with-errors"></div>
          </div>

          <button type="submit" class="btn-green btn-block">XÁC NHẬN</button>
        </form>
      </div>
    </div>
  </div>
</div>