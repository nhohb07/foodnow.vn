<?php 
  $bodyAttrs = 'data-spy="scroll" data-target="#menu-tab-switcher" data-offset="100"';
  include '_header.php';

  $menuLeftList = [
    'TRÀ SỮA','TRÀ NGUYÊN CHẤT','COFFEE','TRÀ SỮA ĐẶC BIỆT','SODA Ý','ĐÁ XAY','SỮA CHUA'
  ];
?>

<script src="lib/PgwSlider/pgwslider.js"></script>
<script src="lib/jquery.stickOnScroll.js"></script>
<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53ce5e8d206bbf44"></script>  -->
<script src="lib/colorbox/jquery.colorbox-min.js"></script>
<!-- Pick A Date -->
<script src="lib/pickadate.js/lib/picker.js"></script>
<script src="lib/pickadate.js/lib/picker.date.js"></script>
<!-- Chosen JQuery -->
<!-- <script src="lib/chosen.jquery.min.js"></script> -->

<link rel="stylesheet" href="lib/colorbox/example3/colorbox.css">

<div class="page page-detail">
  <div class="container">
    <div class="breadcrumbs">
      Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower
    </div>

    <section class="basic-info">
      <div class="row">
        <div class="col-xs-6">
          <ul class="pgwSlider">
            <li><img src="img/product-detail-1.jpg" alt="" class="img-responsive"></li>
            <li><img src="img/product-detail-2.jpg" alt="" class="img-responsive"></li>
            <li><img src="img/product-detail-3.jpg" alt="" class="img-responsive"></li>
            <li><img src="img/product-detail-4.jpg" alt="" class="img-responsive"></li>
            <li><img src="img/product-detail-4.jpg" alt="" class="img-responsive"></li>
            <li><img src="img/product-detail-4.jpg" alt="" class="img-responsive"></li>
            <li><img src="img/product-detail-4.jpg" alt="" class="img-responsive"></li>
          </ul>
        </div>
        <div class="col-xs-6">

          <h1 class="basic-info-name">Whisk - Bánh Cheese Tart - Bitexco Tower</h1>
          <div class="store-info">
            <span class="store-name">TIỆM BÁNH - CHÂU Á</span>
            <span class="store-address">83/35 Phạm Văn Bạch, P. 15, Quận Tan Binh, TP.HCM</span>
          </div>
          
          <div>
            <div class="store-review">
              <span class="store-review-star">3,5</span>
              <i class="icon-star"></i>
              <p>15 bình luận</p>
            </div>

            <div class="store-working-time">
              <ul>
                <li>
                  <span class="icon"><i class="icon-time"></i></span>
                  <span class="text">09:30 - 21:00</span>
                </li>
                <li>
                  <span class="icon"><i class="icon-money-circle"></i></span>
                  <span class="text">39,000 - 220,000</span>
                </li>
                <li>
                  <span class="icon"><i class="icon-status active"></i></span>
                  <span class="text">Đang mở cửa</span>
                </li>
              </ul>
            </div>
          </div>

          <div class="sharing-tools">
            <div class="addthis_inline_share_toolbox_znk0"></div>
          </div>

          <div class="store-utils">
            <div class="row">
              <div class="col-xs-4 border-right">
                <h3>THANH TOÁN</h3>
                <div class="util-content">
                  <img src="img/payment-jcb.png" alt="JCB">
                  <img src="img/payment-visa.png" alt="VISA">
                  <img src="img/payment-other.png" alt="Other">
                </div>
              </div>
              <div class="col-xs-4 border-right">
                <h3>PHÍ VẬN CHUYỂN</h3>
                <div class="util-content">
                  7,000 đ/km
                </div>
              </div>
              <div class="col-xs-4">
                <h3>GIAO HÀNG</h3>
                <div class="util-content">
                  <img src="img/shipping-giaohangnhanh.png" alt="">
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

    <hr>

    <section class="product-info">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">THỰC ĐƠN</a>
        </li>
        <li role="presentation">
          <a href="#introduce" aria-controls="introduce" role="tab" data-toggle="tab">GIỚI THIỆU</a>
        </li>
        <li role="presentation">
          <a href="#special" aria-controls="special" role="tab" data-toggle="tab">KHUYẾN MÃI</a>
        </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="menu">
          <div class="row">
            <div class="col-xs-3">
              <div class="menu-left" id="menu-left" data-stickonscroll="menu-left" data-stickyType="window">
                <div id="menu-tab-switcher">
                  <h3>Menu</h3>
                  <ul class="nav nav-tabs" role="tablist">
                    <?php foreach($menuLeftList as $key => $menu) { ?>
                    <li class="<?=$key == 0 ? 'active' : ''?>"><a href="#<?=$key?>"><?=$menu?></a></li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="menu-content">
                <div class="voucher-code">
                  <div class="pull-left special-code">
                    Giảm 50% - <span>Nhập mã:</span>  CHICKGO50
                  </div>
                  <div class="pull-right">
                    <a href="javascript:void(0)" class="action-copy" onclick="copyToClipboard('CHICKGO50', this)">Copy code</a>
                  </div>
                </div>

                <?php foreach($menuLeftList as $key => $menu) { ?>
                <h4 class="menu-content-heading" id="<?=$key?>"><?=$menu?></h4>
                <div class="menu-content-list">
                  <?php for($i = 0; $i < 4; $i++) { ?>
                  <div class="product-item">
                    <div class="row">
                      <div class="col-xs-2">
                        <a href="img/product-detail-3.jpg" class="menu-images" title="">
                          <img src="img/product-3.png" id="product-image-<?=$key.$i?>" alt="" class="img-responsive product-image" width="77" height="53">
                        </a>
                      </div>
                      <div class="col-xs-7">
                        <h2 class="product-name">Chicken Go Saigon - Shop Online</h2>
                        <div class="product-desc">Whisk - Bánh Cheese Tart - Bitexco Tower</div>
                        <div class="product-purchased">Đã được đặt <b>151</b> lần</div>
                      </div>
                      <div class="col-xs-3 text-right">
                        <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#addCartModal" onclick="flyToElement('#product-image-<?=$key.$i?>', '#cart-fly-here')" class="column-right"> -->
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#addCartModal" class="column-right">
                          <span class="product-price">30,000 đ</span>
                          <span class="button-add-cart"><i class="icon-plus"></i></span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="col-xs-3">
              <div class="menu-right" id="menu-right" data-stickonscroll="menu-right" data-stickyType="window">
                <div class="cart">
                  <div class="cart-head">
                    <div class="cart-head-left" id="cart-fly-here" data-toggle="modal" data-target="#userListModal">4 phần - 1 người</div>
                    <div class="cart-head-right">
                      <a data-toggle="modal" data-target="#shareModal" class="cart-head-group">Nhóm cùng đặt</a>
                      <a href="" class="cart-head-delete">Xóa</a>
                    </div>
                  </div>
                  <div class="cart-body">
                    <div class="scroll-wrapper">
                      <?php for($u = 0; $u < 2; $u++) { ?>
                      <div class="user">
                        <div class="user-avatar"></div>
                        <div class="user-name">Toàn Nguyễn</div>
                        <div class="user-quantity">4 món</div>
                      </div>

                      <div class="products">
                        <?php for($i = 0; $i < 3; $i++) { ?>
                        <div class="product-item">
                          <div class="product-row">
                            <div class="product-quantity">
                              <span class="quantity-button quantity-increase"><i class="icon-plus"></i></span>
                              <span class="quantity-text">1</span>
                              <span class="quantity-button quantity-decrease"><i class="icon-minus"></i></span>
                            </div>
                            <div class="product-name">Gà ta bó xôi chà bông</div>
                          </div>
                          <div class="product-row">
                            <div class="product-note">
                              <input type="text" placeholder="Ghi chú">
                            </div>
                            <div class="product-price">30,000 đ</div>
                          </div>
                        </div>
                        <?php } ?>

                      </div>
                      <?php } ?>
                    </div>

                    <div class="cart-footer">
                      <div class="cart-footer-row cart-total">
                        <div class="pull-left">Cộng</div>
                        <div class="pull-right"><b>120,000 đ</b></div>
                      </div>
                      <div class="cart-footer-row cart-shipping">
                        <div class="pull-left">Phí vận chuyển (Est.)</div>
                        <div class="pull-right"><span>5,000 đ/km</span></div>
                      </div>
                      <div class="cart-footer-row">
                        <p class="help text-center"> Nhập mã khuyến mãi ở bước hoàn tất</p>
                      </div>
                      <div class="cart-footer-row cart-estimate">
                        <div class="pull-left">Tạm tính</div>
                        <div class="pull-right"><b>125,000 đ</b></div>
                      </div>
                      <div class="cart-button">
                        <button data-toggle="modal" data-target="#orderModal">đặt trước</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="introduce">
          <div class="text-center" style="padding: 50px 0;">
            Hiện tại chưa có bài giới thiệu cho địa điểm này
          </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="special">
          <div class="text-center" style="padding: 50px 0;">
            Hiện tại chưa có bài giới thiệu cho địa điểm này
          </div>
        </div>
      </div>
    </section>
    
    <section class="related">
      <h2 class="section-heading">Có thể bạn quan tâm</h2>
      <div class="product-list">
        <div class="row">
          <?php for($i = 0; $i < 5; $i++) { ?>
          <div class="col-xs-5ths product-item">
            <a href="">
              <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                  <div class="active item">
                    <img src="img/product-1.png" class="img-responsive center-block">
                  </div>
                  <div class="item">
                    <img src="img/product-2.png" class="img-responsive center-block">
                  </div>
                  <div class="item">
                    <img src="img/product-3.png" class="img-responsive center-block">
                  </div>
                  <div class="item">
                    <img src="img/product-4.png" class="img-responsive center-block">
                  </div>
                </div>
              </div>
              <h2 class="product-name">Chicken Go Saigon - Shop Online</h2>
              <span class="product-address">83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, TP.HCM</span>
            </a>
          </div>
          <?php } ?>
        </div>
      </div>
    </section>
  </div>
</div>

<!-- Share Modal -->
<?php include '_modal-share.php'; ?>

<!-- User List Modal -->
<?php include '_modal-user-list.php'; ?>

<!-- User List Modal -->
<?php include '_modal-add-cart.php'; ?>

<!-- Order Modal -->
<?php include '_modal-order.php'; ?>

<script>
  $(document).ready(function() {
    const pgwSliderInst = $('.pgwSlider').pgwSlider({
      listPosition: 'left',
      beforeSlide: function(index) {
        nextPage(index);
        // if(index === 1 || index % 6 === 0) {
        //   $('.ps-list-control.ps-list-next').trigger('click');
        // }
      }
    });

    pgwSliderInst.plugin.find('.ps-list').wrap('<div class="ps-list-container"></div>');

    var psListContainerObject = pgwSliderInst.plugin.find('.ps-list-container');
    var psListObject = pgwSliderInst.plugin.find('.ps-list');
    var psListObjectTotal = pgwSliderInst.plugin.find('.ps-list>li').length;

    var psListContainerHeight = psListContainerObject.height();
    var psListHeight = psListObject.height();
    var currentTopPosition = 0;
    var pageLimit = 5;
    var totalPage = 0;
    if (psListObjectTotal % pageLimit !== 0) {
      totalPage = parseInt(psListObject.find('li').length / pageLimit) + 1;
    } else {
      totalPage = psListObjectTotal / pageLimit;
    }
    var page = 1;

    if(psListContainerHeight < psListHeight) {
      pgwSliderInst.plugin.find('.ps-list-container').prepend('<div class="ps-list-control ps-list-prev"><span class="icon-arrow-up"></span></div>');
      pgwSliderInst.plugin.find('.ps-list-container').append('<div class="ps-list-control ps-list-next"><span class="icon-arrow-down"></span></div>');
    }

    $('.ps-list-control.ps-list-prev').click(function() {
      if(page === 1) {
        page = totalPage;
      } else {
        page = page - 1;
      }

      currentTopPosition = (psListContainerHeight - 74) * (page - 1);

      psListObject.animate({
        top: currentTopPosition * -1
      }, 500);
    });

    $('.ps-list-control.ps-list-next').click(function() {
      if(page === totalPage) {
        page = 1;
      } else {
        page = page + 1;
      }

      currentTopPosition = (psListContainerHeight - 74) * (page - 1);

      psListObject.animate({
        top: currentTopPosition * -1
      }, 500);
    });

    function nextPage(index) {
      if(index === 1 && page === totalPage) {
        $('.ps-list-control.ps-list-next').trigger('click');
        return;
      }

      if(index === (page * pageLimit + 1)) {
        $('.ps-list-control.ps-list-next').trigger('click');
      }
    }
  });

  $('#menu-left, #menu-right').stickOnScroll({
    topOffset: $('body>header').outerHeight() + 10,
    setParentOnStick: true,
    setWidthOnStick: true,
    footerElement: $('.related'),
  });

  $(window).scroll(function() {
    if($('#menu-tab-switcher>ul>li.active').length === 0) {
      $('#menu-tab-switcher>ul>li').first().addClass('active');
    }
  });

  $(".menu-images").colorbox({rel:'group3', transition:"elastic"});

  function copyShareLink(element) {
    $(element).select();
    copyToClipboard($(element).val(), this);
    toastr.success('Đã copy thành công!', '', {timeOut: 3000});
  }
</script>

<?php include '_footer.php' ?>