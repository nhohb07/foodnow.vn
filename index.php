<?php include '_header.php' ?>
  
  <section class="section-header">
    <img src="img/home-header.jpg" alt="" class="img-responsive">
    
    <div class="sh-center">
      <h2 class="sh-heading"><b>THƯỢNG ĐẾ!</b> Hãy gọi món đi ạ.</h2>
      <div class="sh-search-box">
        <form>
          <input type="text" placeholder="Tìm món ưa thích ngay..." id="search-keyword">
          <button class="sh-btn-search">
            <i class="icon-search"></i>
          </button>
        </form>

        <div class="search-suggestion" style="display: none;">
          <a href="">
            <div class="row">
              <div class="col-xs-2">
                <img src="img/product-1.png" alt="" class="img-responsive">
              </div>
              <div class="col-xs-7">
                <h2>Buffet tối 60 món Việt Nam, Á, Âu, lẩu tại Buffet</h2>
                <div class="product-address">
                  83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, Tp.HCM
                </div>
              </div>
              <div class="col-xs-3 text-right">
                <div class="open-status">
                  <span class="open-status-text">Chưa mở cửa</span>
                  <span class="open-status-icon"></span>
                </div>
              </div>
            </div>
          </a>
          <a href="">
            <div class="row">
              <div class="col-xs-2">
                <img src="img/product-1.png" alt="" class="img-responsive">
              </div>
              <div class="col-xs-8">
                <h2>Buffet tối 60 món Việt Nam, Á, Âu, lẩu tại Buffet</h2>
                <div class="product-address">
                  83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, Tp.HCM
                </div>
              </div>
              <div class="col-xs-2 text-right">
                <div class="open-status active">
                  <span class="open-status-text">Mở cửa</span>
                  <span class="open-status-icon"></span>
                </div>
              </div>
            </div>
          </a>
          <div class="seach-suggestion-more">
            <a href="search.php?q=mon">
              <i class="icon-search"></i>
              <span>Tìm thêm kết quả cho <strong>"Món"</strong></span>
            </a>
          </div>
        </div>

      </div>
    </div>
  </section>
  
  <div class="container">
    <section class="section-butttons">
      <a onclick="initOwl('morning')" class="button rounded sb-btn-morning">Món Sáng</a>
      <a onclick="initOwl('afternoon')" class="button rounded sb-btn-afternoon">Món Xế</a>
      <a onclick="initOwl('evening')" class="button rounded sb-btn-evening">Món Tối</a>
      <a onclick="initOwl('package')" class="button rounded sb-btn-package">Hàng Đóng Gói <i class="icon-package"></i></a>
    </section>
  </div>

  <div class="container">
    <div class="owl-carousel" id="home-owl-slider">
      
    </div>
  </div>

  <section class="section-most-order">
    <div class="container">
      <div class="s-header">
        <div class="row">
          <div class="col-xs-7">
            <h2>Đang được đặt nhiều</h2>
          </div>
          <div class="col-xs-5">
            <div class="pull-right">
              <a href="" class="button rounded s-header-btn active">Tất Cả</a>
              <a href="" class="button rounded s-header-btn">NHÀ HÀNG</a>
              <a href="" class="button rounded s-header-btn">ĐÓNG GÓI</a>
            </div>
          </div>
        </div>
      </div>
      <div class="s-content">
        <img src="img/qc1.jpg" alt="" class="img-responsive">

        <div class="product-list">
          <div class="row">
            <?php for($i = 0; $i < 8; $i++) { ?>
            <div class="col-xs-3 product-item">
              <a href="">
                <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <div class="active item">
                      <img src="img/product-1.png" class="img-responsive center-block">
                    </div>
                    <div class="item">
                      <img src="img/product-2.png" class="img-responsive center-block">
                    </div>
                    <div class="item">
                      <img src="img/product-3.png" class="img-responsive center-block">
                    </div>
                    <div class="item">
                      <img src="img/product-4.png" class="img-responsive center-block">
                    </div>
                  </div>
                </div>
                <h2 class="product-name">Chicken Go Saigon - Shop Online</h2>
                <span class="product-address">83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, TP.HCM</span>
              </a>
            </div>
            <?php } ?>
          </div>
        </div>

        <div class="s-footer">
          <a href="" class="button rounded button-view-more">
            <span class="button-text">Xem thêm</span>
            <span class="button-dot"></span>
            <span class="button-dot"></span>
            <span class="button-dot"></span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="section-most-place">
    <div class="container">
      <div class="s-header">
        <div class="row">
          <div class="col-xs-7">
            <h2>Địa điểm phổ biến</h2>
          </div>
          <div class="col-xs-5">
            <div class="pull-right">
              <a href="" class="button rounded s-header-btn active">Tất Cả</a>
              <a href="" class="button rounded s-header-btn">NHÀ HÀNG</a>
              <a href="" class="button rounded s-header-btn">ĐÓNG GÓI</a>
            </div>
          </div>
        </div>
      </div>
      <div class="s-content">
        <img src="img/qc2.jpg" alt="" class="img-responsive">

        <div class="product-list">
          <div class="row">
            <?php for($i = 0; $i < 8; $i++) { ?>
            <div class="col-xs-3 product-item">
              <a href="">
                <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <div class="active item">
                      <img src="img/product-1.png" class="img-responsive center-block">
                    </div>
                    <div class="item">
                      <img src="img/product-2.png" class="img-responsive center-block">
                    </div>
                    <div class="item">
                      <img src="img/product-3.png" class="img-responsive center-block">
                    </div>
                    <div class="item">
                      <img src="img/product-4.png" class="img-responsive center-block">
                    </div>
                  </div>
                </div>
                <h2 class="product-name">Chicken Go Saigon - Shop Online</h2>
                <span class="product-address">83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, TP.HCM</span>
              </a>
            </div>
            <?php } ?>
          </div>
        </div>

        <div class="s-footer">
          <a href="" class="button rounded button-view-more">
            <span class="button-text">Xem thêm</span>
            <span class="button-dot"></span>
            <span class="button-dot"></span>
            <span class="button-dot"></span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="get-started">
    <div class="container">
      <h2>3 bước để trở thành Thượng Đế !</h2>
      <div class="row">
        <div class="col-xs-4">
          <div class="gs-wrapper">
            <div class="gs-icon"><i class="icon-marker"></i></div>
            <div class="gs-step">
              <strong>1. Chọn địa điểm</strong>
              <span>Thực đơn đa dạng, hấp dẫn</span>
            </div>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="gs-wrapper">
            <div class="gs-icon"><i class="icon-bag-2"></i></div>
            <div class="gs-step">
              <strong>2. Chọn & đặt món</strong>
              <span>Chọn & đặt món yêu thích tiện lợi, dễ dàng</span>
            </div>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="gs-wrapper">
            <div class="gs-icon"><i class="icon-check"></i></div>
            <div class="gs-step">
              <strong>3. Nhận & thanh toán</strong>
              <span>Giao hàng trong ngày với hầu hết các món ăn</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    $(window).scroll(function(){
      var aTop = $('.section-header').height();

      if($(this).scrollTop() < 280){
        $('.h-search-box').hide();
        $('.header-help').show();
      } else {
        $('.header-help').hide();
        $('.h-search-box').show();
      }
    });

    $(window).scroll();

    $('#search-keyword').keyup(function() {
      if($(this).val() !== '') {
        $('.search-suggestion').show();
      } else {
        $('.search-suggestion').hide();
      }
    });

  </script>

<?php include '_footer.php' ?>