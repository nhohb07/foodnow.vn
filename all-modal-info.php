<?php include '_header.php' ?>

<div class="page page-profile page-info">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      
      <h1 class="page-title">Modal info</h1>

      <div class="content-wrapper">

        <button class="btn btn-primary" data-toggle="modal" data-target="#welcomeModal">Xin chào!</button>
        <button class="btn btn-primary" data-toggle="modal" data-target="#maintainModal">Xin lỗi! Hệ thống đang bảo trì</button>
        <button class="btn btn-primary" data-toggle="modal" data-target="#congratModal">Chúc mừng bạn đã đặt món thành công!</button>
        <button class="btn btn-primary" data-toggle="modal" data-target="#infoModalGreen">Chào bạn đã đến với FoodNow! (header green)</button>
        <button class="btn btn-primary" data-toggle="modal" data-target="#infoModal">Chào bạn đã đến với FoodNow! (header white)</button>

      </div>

    </div>
  </div>
</div>

<!-- Modal Welcome -->
<div class="modal fade modal-header-transparent info-modal welcome-modal modal-640" id="welcomeModal" tabindex="-1" role="dialog" aria-labelledby="welcomeModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">

          <div class="welcome-modal-header">
            <img src="img/modal-info-1-header.png" alt="" class="img-responsive">
          </div>

          <div class="info-modal-content">
            <h3>Xin chào!</h3>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn-primary" data-dismiss="modal">Đóng</button>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- END Modal Welcome -->

<!-- Modal Maintain -->
<div class="modal fade modal-header-transparent info-modal maintain-modal modal-640" id="maintainModal" tabindex="-1" role="dialog" aria-labelledby="maintainModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">

          <div class="maintain-modal-header">
            <img src="img/maintain.png" alt="" class="img-responsive">
          </div>

          <div class="info-modal-content">
            <h3>Xin lỗi! Hệ thống đang bảo trì</h3>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn-primary" data-dismiss="modal">Đóng</button>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- END Modal Maintain -->

<!-- Modal Congration -->
<div class="modal fade modal-header-transparent info-modal congrat-modal modal-640" id="congratModal" tabindex="-1" role="dialog" aria-labelledby="congratModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">

          <div class="congrat-modal-header">
            <img src="img/done-bg.png" alt="">
          </div>

          <div class="info-modal-content">
            <h3>Chúc mừng bạn đã đặt món thành công!</h3>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn-primary" data-dismiss="modal">Đóng</button>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- END Modal Congration -->

<!-- Modal Info Header Green -->
<div class="modal fade modal-header-transparent info-modal modal-640" id="infoModalGreen" tabindex="-1" role="dialog" aria-labelledby="infoModalGreenLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="info-modal-header">
            <h4>Thông báo</h4>
          </div>
          <div class="info-modal-content">
            <h3>Chào bạn đã đến với FoodNow!</h3>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn-primary" data-dismiss="modal">Đóng</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END Modal Info Header Green -->

<!-- Modal Info Header White -->
<div class="modal fade modal-header-transparent info-modal modal-640" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="info-modal-header">
            <h4>
              Thông báo
              <hr>
            </h4>
          </div>
          <div class="info-modal-content">
            <h3>Chào bạn đã đến với FoodNow!</h3>
            <p>Dịch vụ hoàn hảo là mục tiêu <b>FoodNow</b> hướng đến. Tuy vậy trong lúc phục vụ quý khách, nếu có sơ suất và vấn đề xảy ra, quý khách vui lòng hỗ trợ chúng tôi bằng cách gửi Email Feedback vào <b>info@foodnow.vn</b></p>
            <p>Trường hợp quý khách đăng lên Facebook, quý khách có thể tag thêm <b>@foodnow.vn</b> để đội ngũ chăm sóc khách hàng có thể xử lý vấn đề của bạn nhanh hơn.</b>
          </div>
          <div class="info-modal-footer text-right">
            <button class="btn-primary" data-dismiss="modal">Đóng</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END Modal Info Header White -->

<script type="text/javascript">
  $(document).ready(function () {
    setTimeout(function() {
      $('#welcomeModal').modal('show');
    }, 500);
  });
</script>

<?php include '_footer.php' ?>