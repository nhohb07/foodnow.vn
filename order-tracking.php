<?php include '_header.php' ?>

<div class="page page-profile page-order-tracking">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » Tra cứu đơn hàng</div>

    <div class="page-wrapper">
      
      <h1 class="page-title">Tra cứu đơn hàng</h1>
      
      <!-- Search form -->
      <div class="content-wrapper">

        <form class="form-horizontal" action="">
          <div class="form-group">
            <label for="inputEmail3" class="col-xs-4 control-label">Mã đơn hàng</label>
            <div class="col-xs-4">
              <input type="email" class="form-control" placeholder="Email">
              <span class="help-block error">Mã đơn hàng không đúng!</span>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-xs-4 control-label">Số điện thoại</label>
            <div class="col-xs-4">
              <input type="email" class="form-control" placeholder="Không có">
              <span class="help-block error"></span>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-xs-4 control-label">Mã bảo mật</label>
            <div class="col-xs-3">
              <input type="email" class="form-control">
              <span class="help-block error"></span>
            </div>
            <div class="col-xs-1">
              <img src="img/order-search-captcha.png" alt="" class="img-responsive captcha-img">
            </div>
          </div>

          <div class="form-group">
          <div class="col-xs-4"></div>
          <div class="col-xs-2">
            <a href="" class="link">Bỏ qua</a>
          </div>
          <div class="col-xs-2 text-right">
            <button class="btn btn-primary">Kiểm tra</button>
          </div>
          </div>
        </form>

      </div>

      
      <!-- Show search result -->
      <div class="order-tracking-result">
        <div class="order-history-list">
          <table class="table">
            <thead>
              <tr>
                <th class="nowrap">Mã đơn hàng</th>
                <th>Ngày đặt</th>
                <th>Nơi đặt</th>
                <th width="150">Tổng tiền</th>
                <th>Trạng thái</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>3454657</b></td>
                <td>
                  <p>Đặt món: 22/10/2017 10:00</p>
                  <p>Giao hàng: 22/10/2017 10:00</p>
                </td>
                <td>
                  <div class="text-wrapper">
                    <p><b>Cửa hàng bánh Gia Phúc</b></p>
                    <p>360 Lý Thái Tổ, P. 1, Quận 3, TP. HCM</p>
                  </div>
                  <div class="action-wrapper">
                    <a href="" class="link">Đặt hàng lại</a>
                  </div>
                </td>
                <td>
                  <div class="text-wrapper">
                    <p><b class="red">219.000 VND</b></p>
                    <p>13 món (Giao hàng nhận tiền)</p>
                  </div>
                  <div class="action-wrapper">
                    <a href="" class="link">In hóa đơn</a>
                  </div>
                </td>
                <td>
                  <div class="text-wrapper">
                    <p>Đã giao hàng</p>
                  </div>
                  <div class="action-wrapper">
                    <a href="profile-order-detail.html" class="link">Xem chi tiết</a>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      
      
      <!-- Show below when result null -->
      <div class="empty-order">
        <img src="img/order-search-empty.png" alt="" class="img-responsive center-block">
        <h2>Đơn hàng không tồn tại hoặc đang được xử lý!</h2>
        <p>Email: cskh@foodnow.vn  -  Hotline: 1900 1901</p>
      </div>

    </div>
  </div>
</div>

<?php include '_footer.php' ?>