<?php include '_header.php' ?>

<div class="page page-profile page-info">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      
      <h1 class="page-title">Hướng dẫn thanh toán</h1>

      <div class="content-wrapper">
        <h2>Cách phương thức thanh toán được áp dụng trên FoodNow</h2>
        <p>- Thanh toán tiền mặt khi nhận hàng (COD)</p>
        <p>- Thanh toán qua thẻ Debit/Credit VISA/MASTER/JCB</p>
        <p>- Thanh toán qua thẻ ATM nội địa​</p>
        <p>- Thanh toán qua Ví AirPay (AirPay e-Wallet)</p>
        <p>- Thanh toán qua PayNow Credits</p>

        <img src="img/instruction-payment-method.png" alt="" class="img-responsive center-block img-border" width="415">
        
        <h2>Cách thanh toán qua thẻ Debit/Credit VISA/MASTER/JCB</h2>
        <p>Để sử dụng hình thức này, trước tiên khách hàng cần thêm thông tin thẻ trong mục "Quản lý thẻ Visa/Master/JCB". Khách hàng cũng có thể cài đặt "Yêu cầu mật khẩu" cho mỗi giao dịch được thanh toán bằng hình thức này để nâng cao tính bảo mật và an toàn cho thông tin thẻ. Để sử dụng hình thức này, trước tiên khách hàng cần thêm thông tin thẻ trong mục "Quản lý thẻ Visa/Master/JCB". Khách hàng cũng có thể cài đặt "Yêu cầu mật khẩu" cho mỗi giao dịch được thanh toán bằng hình thức này để nâng cao tính bảo mật và an toàn cho thông tin thẻ.</p>

        <img src="img/instruction-order.png" alt="" class="img-responsive center-block" width="586">        

        <p>Để sử dụng hình thức này, trước tiên khách hàng cần thêm thông tin thẻ trong mục "Quản lý thẻ Visa/Master/JCB". Khách hàng cũng có thể cài đặt "Yêu cầu mật khẩu" cho mỗi giao dịch được thanh toán bằng hình thức này để nâng cao tính bảo mật và an toàn cho thông tin thẻ. Để sử dụng hình thức này, trước tiên khách hàng cần thêm thông tin thẻ trong mục "Quản lý thẻ Visa/Master/JCB". </p>
        <p>Trước tiên khách hàng cần thêm thông tin thẻ trong mục "Quản lý thẻ Visa/Master/JCB". Khách hàng cũng có thể cài đặt "Yêu cầu mật khẩu" cho mỗi giao dịch được thanh toán bằng hình thức này để nâng cao tính bảo mật và an toàn cho thông tin thẻ. Để sử dụng hình thức này, trước tiên khách hàng cần thêm thông tin thẻ trong mục "Quản lý thẻ Visa/Master/JCB". </p>
      </div>

    </div>
  </div>
</div>

<?php include '_footer.php' ?>