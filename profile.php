<?php include '_header.php' ?>

<div class="page page-profile">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      
      <?php include '_profile-sidebar.php';?>

      <div class="page-content">
        <h1 class="page-title">THÔNG TIN CÁ NHÂN</h1>

        <div class="content-wrapper profile-wrapper">

          <form action="" class="form-horizontal">
            
            <!-- Avatar -->
            <div class="form-group">
              <label class="col-xs-4 control-label"></label>
              <div class="col-xs-5">
                <a>
                  <img src="img/icon-avatar.png" alt="" class="img-responsive icon-avatar">
                  <span class="avatar-text">Ảnh đại diện</span>
                </a>
              </div>
            </div>
            
            <!-- Email -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Địa chỉ email:</label>
              <div class="col-xs-5">
                <input type="email" class="form-control" value="weedsbrand@gmail.com">
              </div>
            </div>
            
            <!-- Phone -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Điện thoại di động:</label>
              <div class="col-xs-5">
                <input type="text" class="form-control" placeholder="Không có">
              </div>
            </div>
            
            <!-- Name -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Tên:</label>
              <div class="col-xs-5">
                <input type="text" class="form-control" value="Nguyễn Toàn">
              </div>
            </div>

            <!-- Gender -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Giới tính:</label>
              <div class="col-xs-5">
                <select class="form-control">
                  <option value="">Tất cả</option>
                  <option value="1">Nam</option>
                  <option value="0">Nữ</option>
                </select>
              </div>
            </div>

            <!-- Birthday -->
            <div class="form-group">
              <label class="col-xs-4 control-label">Ngày sinh:</label>
              <div class="col-xs-5">
                <div class="row">
                  <div class="col-xs-4">
                    <select class="form-control">
                      <option value="" disabled selected hidden>Ngày</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </select>
                  </div>
                  <div class="col-xs-4">
                    <select class="form-control">
                      <option value="" disabled selected hidden>Tháng</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </select>
                  </div>
                  <div class="col-xs-4">
                    <select class="form-control">
                      <option value="" disabled selected hidden>Năm</option>
                      <option value="2017">2017</option>
                      <option value="2016">2016</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-xs-4"></div>
              <div class="col-xs-5">
                <a href="change-password.html" class="link link-change-password">Đổi mật khẩu</a>

                <button class="btn btn-primary pull-right" type="submit">Lưu thay đổi</button>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_footer.php' ?>