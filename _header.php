<?php $fileName = basename($_SERVER['PHP_SELF'], '.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>www.foodnow.vn</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="lib/OwlCarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="lib/toastr/toastr.min.css">
    <link rel="stylesheet" href="lib/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/<?=$fileName?>.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="lib/toastr/toastr.min.js"></script>
  </head>
  <body <?=isset($bodyAttrs) ? $bodyAttrs : null?>>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1261715420604681";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    <header>
      <div class="container">
        <div class="row">

          <div class="col-xs-10 col-center">
            <div class="logo">
              <a href=""><img src="img/logo-1.png" alt=""></a>
            </div>

            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle city-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                TP Hồ Chi Minh <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#" class="active">TP. Hồ Chí Minh</a></li>
                <li><a href="#">Hà Nội</a></li>
                <li><a href="#">Đà Nẵng</a></li>
                <li><a href="#">Cần Thơ</a></li>
                <li><a href="#">Huế</a></li>
                <li><a href="#">Hải Phòng</a></li>
              </ul>
            </div>
            <div class="header-help <?=($fileName != 'index') ? 'hidden' : ''?>">
              <i class="icon-arrow-instruction"></i>
              <span class="text">
                Chọn địa điểm của bạn để được phục vụ nhanh nhất
              </span>
            </div>

            <div class="h-search-box" style="<?=($fileName == 'index') ? 'display: none' : ''?>">
              <form>
                <input type="text" placeholder="Tìm món ưa thích ngay...">
                <button class="sh-btn-search">
                  <i class="icon-search"></i>
                </button>
              </form>
            </div>
          </div>

          <div class="col-xs-2 col-right">
            <div class="pull-right">
              <a class="notifications">
                <i class="icon-bag"></i>
                <span class="notification-badge">3</span>
              </a>
              
              <!-- Add class "hidden" to hide this button -->
              <a class="btn hidden" href="#" role="button" data-toggle="modal" data-target="#loginModal">ĐĂNG NHẬP</a>
              
              <!-- Remove class "hidden" to show this button -->
              <div class="btn-group-profile">
                <button type="button" class="btn btn-default dropdown-toggle city-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="img/avatar-icon.png" alt="">
                  <span class="ellipsis">Toan Nguyen</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li>
                    <a href="profile-order-history.html">
                      <i class="icon-order-history"></i>
                      Lịch sử đặt món
                    </a>
                  </li>
                  <li>
                    <a href="profile.html">
                      <i class="icon-setting"></i>
                      Cập nhật tài khoản
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="icon-logout"></i>
                      Thoát
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
    </header>

    <main>      
  