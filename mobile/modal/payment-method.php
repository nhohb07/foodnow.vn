<div class="modal fade modal-payment-method" id="modalPaymentMethod" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'PHƯƠNG THỨC THANH TOÁN'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">

        <h5 class="modal-heading">Thẻ tín dụng / Thẻ ghi nợ</h5>
        
        <div class="payment-item">
          <table class="table">
            <thead>
              <tr>
                <td>Card Type</td>
                <td>Ending In</td>
                <td>Expiration Date</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="../img/visa.png" alt="" class="img-responsive"></td>
                <td>0309</td>
                <td>04/20</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="3">
                  <button class="btn btn-block btn-transparent">Xóa</button>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>

      </div>
    </div>
  </div>
</div>