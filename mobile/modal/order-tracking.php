<div class="modal fade modal-order-tracking" id="modalOrderTracking" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'TRA CỨU ĐƠN HÀNG'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">
        <form action="" data-toggle="validator">

          <!-- Order ID -->
          <div class="form-group">
            <label class="control-label">Mã đơn hàng:</label>
            <input type="text" value="54364567" class="form-control" required>
          </div>
            
          <!-- Phone -->
          <div class="form-group">
            <label class="control-label">Số điện thoại</label>
            <input type="text" class="form-control">
          </div>

          <!-- Captcha -->
          <div class="form-group">
            <label class="control-label">Mã bảo mật</label>
            <div class="row row-small-space">
              <div class="col-xs-8">
                <input type="text" class="form-control">
              </div>
              <div class="col-xs-4">
                <img src="../img/order-search-captcha.png" alt="" class="img-responsive center-block">
              </div>
            </div>
          </div>

          <div class="link">
            <a data-dismiss="modal">Bỏ qua</a>
          </div>

          <button class="btn btn-block btn-green btn-normal" type="submit">Kiểm tra</button>

        </form>

        <div class="result empty">
          <img src="../img/order-search-empty.png" alt="" width="97">
          <h5>Đơn hàng không tồn tại hoặc đang được xử lý!</h5>
          <p>Email: cskh@foodnow.vn</p>
          <p>Hotline: 1900 1901</p>
        </div>

        <div class="result order-list">
          <div class="order-item">
            
            <div class="custom-row order-item-row">
              <div class="pull-left">
                <b>Đơn hàng #3454657</b>
              </div>
              <div class="pull-right">
                <span class="text-gray">Đã giao hàng</span>
              </div>
            </div>

            <div class="order-item-row">
              <p>Đặt món: 22/10/2017 10:00</p>
              <p>Giao hàng: 22/10/2017 10:00</p>
            </div>

            <div class="store-info order-item-row">
              <div><b>Cửa hàng bánh Gia Phúc</b></div>
              <div class="text-gray">360 Lý Thái Tổ, P. 1, Quận 3, TP. HCM</div>
            </div>

            <div class="order-item-row">
              <div class="text-red"><b>219.000 VND</b></div>
              <div class="text-gray">13 món (Giao hàng nhận tiền)</div>
            </div>

            <div class="row row-small-space">
              <div class="col-xs-4">
                <a href="" class="text-blue">Đặt hàng lại</a>
              </div>
              <div class="col-xs-4">
                <a href="" class="text-blue">In hóa đơn</a>
              </div>
              <div class="col-xs-4">
                <a href="" class="text-blue">Xem chi tiết</a>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>