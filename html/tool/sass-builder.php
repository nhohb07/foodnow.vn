<?php
$watchDir = isset($argv[1]) ? $argv[1] : '';
$watchFile = isset($argv[2]) ? $argv[2] : '';

$dir = dirname(__DIR__) . '/' . $watchDir;
$buildCommand = "sass --no-cache -t compressed --sourcemap=auto #sassFile #cssFile";
$commandLineMask = "%3s | %-30s | %-30s | %-20s\n";

echo "Current dir: {$dir}\n";
echo "Command: {$buildCommand}\n";
echo str_repeat('_', 20).' Building... '.str_repeat('_', 20)."\n";
printf($commandLineMask, '#', '#sassFile', '#cssFile', '#time');

if(!$watchFile) {
  //get all sass file without _ prefix
  $sassFiles = glob($dir . 'scss/[!_]*.scss');
} else {
  $sassFiles = glob($dir . "scss/{$watchFile}.scss");
}

//build all sass file
foreach($sassFiles as $index => $file) {
  $startTime = microtime(TRUE);
  
  $fileName = basename($file, '.scss');
  
  shell_exec(strtr($buildCommand, [
    '#sassFile' => $file,
    '#cssFile' => "{$dir}css/{$fileName}.css"
  ]));
  $buildedTime = microtime(TRUE) - $startTime;

  printf($commandLineMask, $index + 1, "{$fileName}.scss", "{$fileName}.css", $buildedTime);
}

echo str_repeat('_', 20).' Builded! '.str_repeat('_', 20)."\n\n";