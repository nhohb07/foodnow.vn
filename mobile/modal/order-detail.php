<div class="modal fade modal-order-detail" id="modalOrderDetail" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Chi tiết đơn hàng'; include 'modal-header.php'; ?>

      <div class="modal-body scrollable">

        <div class="overview">

          <div class="custom-row">
            <div class="pull-left">
              Đơn hàng #396113597
            </div>
            <div class="pull-right">
              15/08/2017
            </div>
          </div>

          <div class="total-price">Tổng tiền: 219.000 VND</div>
        </div>

        <div class="shipper">Giao ngày 16/08/2017 bởi <b>Hà Linh</b></div>

        <div class="store-name">Được bán bởi <a href="">Thành Long quán</a></div>
        
        <hr>
        
        <!-- Order status: Processing -->
        <div class="order-process">
          <h5 class="heading">Đang xử lý</h5>

          <div class="progress-wrapper">
            
            <!-- Add or remove class active for current step -->
            <div class="progress-item active">
              <div class="progress-item-wrapper">
                <i class="icon-reload"></i>
              </div>
            </div>
            
            <!-- Add or remove class active for current step -->
            <div class="progress-item text-center">
              <div class="progress-item-wrapper">
                <i class="icon-moto"></i>
              </div>
            </div>
            
            <!-- Add or remove class active for current step -->
            <div class="progress-item text-right">
              <div class="progress-item-wrapper">
                <i class="icon-check-circle"></i>
              </div>
            </div>

          </div>

          <div class="product custom-row">
            <div class="product-img">
              <img src="../img/m-product-detail.png" alt="" width="90">
            </div>
            <div class="product-info">
              <h5>Chicken Go Saigon</h5>
              <span class="price">219.000 VND</span>
              <span class="multi">x</span>
              <span class="quantity">1</span>
            </div>
          </div>
        </div>
        <!-- END Order status: Processing -->

        <!-- Order status: Canceled -->
        <hr>
        <div class="order-process">
          <h5 class="heading">Đã hủy</h5>

          <div class="progress-wrapper">
            
            <!-- Add or remove class active for current step -->
            <div class="progress-item canceled">
              <div class="progress-item-wrapper">
                <i class="icon-close-circle"></i>
              </div>
            </div>
            
            <!-- Add or remove class active for current step -->
            <div class="progress-item text-center">
              <div class="progress-item-wrapper">
                <i class="icon-moto"></i>
              </div>
            </div>
            
            <!-- Add or remove class active for current step -->
            <div class="progress-item text-right">
              <div class="progress-item-wrapper">
                <i class="icon-check-circle"></i>
              </div>
            </div>

          </div>

          <div class="product custom-row">
            <div class="product-img">
              <img src="../img/m-product-detail.png" alt="" width="90">
            </div>
            <div class="product-info">
              <h5>Chicken Go Saigon</h5>
              <button class="btn rounded btn-canceled">Đã hủy</button>
            </div>
          </div>
        </div>
        <!-- END Order status: Canceled -->

        <hr>

        <div class="summary">
          <h5 class="heading">Tóm tắt</h5>

          <div class="custom-row">
            <div class="pull-left">Tạm tính</div>
            <div class="pull-right text-right">219.000 VND</div>
          </div>

          <div class="custom-row">
            <div class="pull-left">Phí vận chuyển</div>
            <div class="pull-right text-right">0 VND</div>
          </div>

          <div class="custom-row">
            <div class="pull-left">Khuyến mãi</div>
            <div class="pull-right text-right">0 VND</div>
          </div>

          <div class="custom-row">
            <div class="pull-left"><b>Tổng (Đã bao gồm VAT.)</b></div>
            <div class="pull-right text-right"><b class="text-red">219.000 VND</b></div>
          </div>
          <div class="help small">Thanh toán bằng Thanh toán khi nhận hàng</div>

        </div>

        <hr>

        <div class="shipping-info">
          <h5 class="heading">Địa chỉ giao hàng</h5>

          <p><b>Nguyễn Minh Toàn</b></p>
          <p>81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM</p>
          <p>0969740960</p>
        </div>

        <div class="actions">
          <!-- Add/Remove attr disabled by order status -->
          <button class="action-remove" disabled>Hủy đơn hàng</button>
        </div>

        <button class="btn btn-block btn-other btn-normal rounded btn-small">CÁC ĐƠN HÀNG KHÁC</button>

      </div>
    </div>
  </div>
</div>