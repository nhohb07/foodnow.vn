<div class="modal fade modal-cart-group" id="modalCartGroup" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Đặt món theo nhóm'; include 'modal-header.php'; ?>

      <div class="modal-body">
        
        <div class="row-1">
          <div class="group-store-info">
            <h5 class="group-store-name">Whisk - Bánh Cheese Tart - Bitexco Tower</h5>
            <div class="group-store-address">83/35 Phạm Văn Bạch, P. 15, Quận 12, TP.HCM</div>
          </div>

          <div class="group-total">
            2 người đang đặt | 2 người đã hoàn tất
          </div>

          <div class="group-separator">Người tham gia</div>

          <div class="members">

            <div class="member">
              <div class="custom-row">
                <div class="column-avatar">
                  <img src="../img/icon-user.png" alt="">
                </div>
                <div class="column-right">
                  <div class="name">Toàn Nguyễn</div>
                  <div class="custom-row">
                    <div class="pull-left quantity">1 món</div>
                    <div class="pull-right text-right role">
                      <span class="icon icon-png-owner"></span> Người tạo
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="member">
              <div class="custom-row">
                <div class="column-avatar">
                  <img src="../img/icon-user.png" alt="">
                </div>
                <div class="column-right">
                  <div class="name">Trân Hạo Dân</div>
                  <div class="custom-row">
                    <div class="pull-left quantity">1 món</div>
                    <div class="pull-right text-right role">
                      Bạn bè
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="empty">
            <img src="../img/icon-cheese.png" alt="" width="106">
            <p>Chưa có món nào được đặt</p>
          </div>
        </div>

        <div class="row-2">
          <button class="btn btn-block btn-green rounded btn-small">Đặt món ngay</button>
        </div>

      </div>
    </div>
  </div>
</div>