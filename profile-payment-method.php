<?php include '_header.php' ?>

<div class="page page-profile page-profile-payment-method">
  <div class="container">
    <div class="page-breadcrumb">Trang chủ » TP. HCM » Whisk - Bánh Cheese Tart - Bitexco Tower</div>

    <div class="page-wrapper">
      <?php $active = 'profile-payment-method'; include '_profile-sidebar.php';?>

      <div class="page-content">
        <h1 class="page-title">Phương thức thanh toán</h1>

        <div class="content-wrapper profile-wrapper">

          <h3 class="payment-method-name">Thẻ tín dụng / Thẻ ghi nợ</h3>

          <div class="payment-method-list">
            <table class="table">
              <thead>
                <tr>
                  <td>Card Type</td>
                  <td>Ending In</td>
                  <td>Expiration Date</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><img src="img/visa.png" alt="" class="img-responsive"></td>
                  <td>0309</td>
                  <td>04/20</td>
                  <td class="text-right">
                    <a href="" class="link-remove">Xóa</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_footer.php' ?>