<div class="modal fade modal-cart" id="modalCart" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Giỏ hàng'; include 'modal-header.php'; ?>

      <div class="modal-body">

        <div class="cart cart-head">
          <div class="custom-row">
            <div class="total" data-toggle="modal" data-target="#modalCartGroup">
              <div class="total-text">2 phần - 1 người</div>
            </div>

            <div class="group">
              <button class="btn rounded" data-toggle="modal" data-target="#modalShare">Nhóm cùng đặt</button>
            </div>

            <div class="actions">
              <button>Xóa</button>
            </div>
          </div>
        </div>

        <div class="cart-body">
          <div class="scroll-wrapper">
            <?php for($u = 0; $u < 2; $u++) { ?>
            <div class="user">
              <div class="user-avatar"></div>
              <div class="user-name">Toàn Nguyễn</div>
              <div class="user-quantity">4 món</div>
            </div>

            <div class="products">
              <?php for($i = 0; $i < 3; $i++) { ?>
              <div class="product-item">
                <div class="product-row">
                  <div class="product-quantity">
                    <span class="quantity-button quantity-increase"><i class="icon-plus"></i></span>
                    <span class="quantity-text">1</span>
                    <span class="quantity-button quantity-decrease"><i class="icon-minus"></i></span>
                  </div>
                  <div class="product-name">Gà ta bó xôi chà bông</div>
                </div>
                <div class="product-row">
                  <div class="product-note">
                    <input type="text" placeholder="Ghi chú">
                  </div>
                  <div class="product-price">30,000 đ</div>
                </div>
              </div>
              <?php } ?>

            </div>
            <?php } ?>
          </div>

          <div class="cart-footer">
            <div class="cart-footer-row cart-total">
              <div class="pull-left">Cộng</div>
              <div class="pull-right"><b>120,000 đ</b></div>
            </div>
            <div class="cart-footer-row cart-shipping">
              <div class="pull-left">Phí vận chuyển (Est.)</div>
              <div class="pull-right"><span>5,000 đ/km</span></div>
            </div>
            <div class="cart-footer-row">
              <p class="help text-center"> Nhập mã khuyến mãi ở bước hoàn tất</p>
            </div>
            <div class="cart-footer-row cart-estimate">
              <div class="pull-left">Tạm tính</div>
              <div class="pull-right"><b>125,000 đ</b></div>
            </div>
            <div class="cart-button">
              <button data-toggle="modal" data-target="#modalPay">đặt trước</button>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>