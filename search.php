<?php include '_header.php' ?>

<div class="page page-search">
  <div class="container">
    <div class="header">
      <div class="row">
        <div class="col-xs-6 search-keyword">Sản phẩm liên quan đến từ khóa <b>"Món"</b></div>
        <div class="col-xs-6 text-right">
          <span class="search-result-count">502 kết quả</span>

          <div class="btn-group select-place">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Tất cả địa điểm <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a href="#" class="active">TP. Hồ Chí Minh</a></li>
              <li><a href="#">Hà Nội</a></li>
              <li><a href="#">Đà Nẵng</a></li>
              <li><a href="#">Cần Thơ</a></li>
              <li><a href="#">Huế</a></li>
              <li><a href="#">Hải Phòng</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="content">
      <div class="row">
        <div class="col-xs-3">
          <div class="sidebar">
            <div class="filter-block">
              <div class="filter-header">Khu vực</div>
              <div class="filter-list">
                <ul>
                  <?php for($i = 0; $i < 12; $i++) { ?>
                  <li><label><input type="checkbox"><span>Quận <?=$i + 1?></span></label></li>
                  <?php } ?>
                </ul>
              </div>
            </div>

            <hr>

            <div class="filter-block">
              <div class="filter-header">Ẩm thực</div>
              <div class="filter-list">
                <ul>
                  <?php foreach(['Thổ Nhỹ Kỳ','Campuchia','Úc','Huế','Hà Nội','Trung Hoa','Âu Mỹ','Miền Tây','Cần Thơ'] as $food) { ?>
                  <li><label><input type="checkbox"><span><?=$food?></span></label></li>
                  <?php } ?>
                </ul>
              </div>
            </div>

            <hr>

            <div class="filter-block">
              <div class="filter-header">Phân loại</div>
              <div class="filter-list">
                <ul>
                  <li class="filter-sub-header">Món ăn</li>
                  <?php foreach(['Cơm','Gà xé phay','Cơm văn phòng','Vịt quay','MÌ quảng'] as $food) { ?>
                  <li><label><input type="checkbox"><span><?=$food?></span></label></li>
                  <?php } ?>
                  
                  <li class="filter-sub-header">THỨC UỐNG</li>
                  <?php foreach(['Trà sữa', 'Nuoc ngot'] as $water) { ?>
                  <li><label><input type="checkbox"><span><?=$water?></span></label></li>
                  <?php } ?>
                </ul>
              </div>
            </div>

          </div>  
        </div>

        <div class="col-xs-9">

          <div class="product-list">
            <div class="row">
              <?php for($i = 0; $i < 9; $i++) { ?>
              <div class="col-xs-4 product-item">
                <a href="">
                  <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner">
                      <div class="active item">
                        <img src="img/product-1.png" class="img-responsive center-block">
                      </div>
                      <div class="item">
                        <img src="img/product-2.png" class="img-responsive center-block">
                      </div>
                      <div class="item">
                        <img src="img/product-3.png" class="img-responsive center-block">
                      </div>
                      <div class="item">
                        <img src="img/product-4.png" class="img-responsive center-block">
                      </div>
                    </div>
                  </div>
                  <h2 class="product-name">Chicken Go Saigon - Shop Online</h2>
                  <span class="product-address">83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, TP.HCM</span>
                </a>
              </div>
              <?php } ?>
            </div>
          </div>
          
        </div>
      </div>

      <div class="search-empty">
        <img src="img/search-no-result.png" alt="">
        <p>Không tìm thấy từ khóa liên quan!</p>
      </div>
    </div>

  </div>
</div>

<?php include '_footer.php' ?>