<div class="modal fade modal-forgot-password" id="modalForgotPassword" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'QUÊN MẬT KHẨU'; include 'modal-header.php'; ?>

      <div class="modal-body">
        <div class="form-icon">
          <img src="../img/icon-forgot-password.png" alt="" class="img-responsive center-block" width="70">
        </div>

        <div class="text-help">
          Hãy nhập vào Email hoặc số điện thoại đã đăng ký trước đó để nhận lại mật khẩu mới.
        </div>

        <form action="" onsubmit="return modalOpenOtherModal('#modalForgotPassword', '#modalActivePin')">
          <input type="text" name="username" class="form-control rounded shadow" placeholder="Email hoặc Số điện thoại" required>
          <button type="submit" class="btn-green btn-block">GỬI ĐI</button>
        </form>
      </div>
    </div>
  </div>
</div>