    </main>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <h3>ĐĂNG KÝ NHẬN TIN</h3>
            <div class="f-content">
              <p>Để nhận các thông báo khuyến mại mới nhất</p>

              <div class="news-letter-box">
                <form action="">
                  <input type="text" placeholder="Nhập email của bạn">
                  <button class="nl-submit"><i class="icon-check"></i></button>
                </form>
              </div>
            </div>
          </div>
          
          <div class="col-xs-4">
            <h3>TẢI MIỄN PHÍ FOODNOW APP</h3>
            <div class="f-content">
              <div class="pull-left app-link">
                <a href=""><img src="img/app-store.png" alt=""></a><br>
                <a href=""><img src="img/google-play.png" alt=""></a>
              </div>
              <div class="pull-left qr-code">
                <a href=""><img src="img/qr-code.png" alt=""></a>
              </div>
            </div>
          </div>

          <div class="col-xs-4">
            <h3>LIKE ĐỂ NHẬN ƯU ĐÃI MỚI NHẤT</h3>
            <div class="f-content">
              <div class="fb-page" data-href="https://www.facebook.com/facebook" data-width="100%" data-hide-cover="true" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
            </div>
          </div>
        </div>

        <hr>
        
        <div class="row">
          <div class="col-xs-8">
            <p class="office-address">Công Ty Cổ Phần Foodnow Lầu 8, Tòa nhà Jabes 1, 244 Cống Quỳnh, P. Phạm Ngũ Lão, Quận 1, Tp.HCM</p>
            <p class="copyright">© Copyright 2015. All Rights Reserved.</p>
          </div>
          <div class="col-xs-4">
            <ul class="footer-util-links">
              <li><a href="payment-guide.html">Hướng dẫn thanh toán</a></li>
              <li><a href="order-tracking.html">Tra cứu đơn hàng</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    
    <?php include '_modal-login.php'; ?>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="lib/OwlCarousel/owl.carousel.min.js"></script>
    <!-- <script src="js/jquery.nicescroll.min.js"></script> -->
    <script src="lib/validator.min.js"></script>
    
    <script src="js/script.js"></script>
  </body>
</html>