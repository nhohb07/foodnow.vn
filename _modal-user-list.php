<div class="modal fade modal-header-transparent user-list-modal" id="userListModal" tabindex="-1" role="dialog" aria-labelledby="userListModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close"></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="header">
            <h2>Whisk - Bánh Cheese Tart - Bitexco Tower</h2>
            <span class="store-name">TIỆM BÁNH - CHÂU Á</span>
            <span class="store-address">83/35 Phạm Văn Bạch, P. 15, Quận 10, TP.HCM</span>
          </div>
          <div class="detail">
            <div class="detail-header">TRẠNG THÁI ĐẶT MÓN THEO NHÓM</div>
            <div class="detail-content">
              <table class="table">
                <caption>2 người đang đặt | 2 người đã hoàn tất</caption>
                <thead>
                  <tr>
                    <th>Người tham gia</th>
                    <th>Số lượng</th>
                    <th>Trạng thái</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <img src="img/icon-user.png" alt="">
                      <span class="name">Toàn Nguyễn</span>
                    </td>
                    <td>4 món</td>
                    <td>
                      <span class="icon icon-png-owner"></span> Người tạo
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="img/icon-user.png" alt="">
                      <span class="name">Lan Trần</span>
                    </td>
                    <td>1 món</td>
                    <td>
                      <span class="icon"></span> Bạn bè
                    </td>
                  </tr>
                  <!-- Empty section -->
                  <tr>
                    <td colspan="3" class="no-border">
                      <div class="detail-empty">
                        <img src="img/icon-cheese.png" alt="">
                        <p>Chưa có món nào được đặt</p>
                      </div>
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="3" class="text-right no-border">
                      <button class="btn button-submit">đặt món ngay</button>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
