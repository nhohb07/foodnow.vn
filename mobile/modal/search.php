<div class="modal fade modal-search" id="modalSearch" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php include 'modal-header.php'; ?>

      <div class="modal-body">
        <div class="search-box">
          <input type="text" placeholder="Tìm món ưa thích ngay...">
          <button><i class="icon-search"></i></button>
        </div>
        
        <!-- Add/Remove class hidden -->
        <div class="search-placeholder hidden">
          <i class="icon-search-light"></i>
          <h4>Nhập từ khóa cần tìm!</h4>
        </div>

        <div class="search-result">
          <div class="row row-small-space row-filter">
            <div class="col-xs-6">
              <span class="search-total">30 kết quả</span>
            </div>

            <div class="col-xs-6 text-right">
              <div class="btn-group">
                <button type="button" class="btn rounded dropdown-toggle no-shadow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Tất cả địa điểm <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="#" class="active">TP. Hồ Chí Minh</a></li>
                  <li><a href="#">Hà Nội</a></li>
                  <li><a href="#">Đà Nẵng</a></li>
                  <li><a href="#">Cần Thơ</a></li>
                  <li><a href="#">Huế</a></li>
                  <li><a href="#">Hải Phòng</a></li>
                </ul>
              </div>
            </div>
          </div>
          
          <!-- Add/Remove class hidden -->
          <div class="search-products">
            <div class="row-products">
              <?php for($i = 0; $i < 5; $i++) { ?>
              <div class="product-item">
                <a href="">
                  <div class="row row-small-space">
                    <div class="col-xs-4">
                      <img src="../img/m-product-detail.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-8">
                      <h2>Chicken Go Saigon Shop Online</h2>
                      <p>83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, TP.HCM</p>
                      <div class="store-status">Chưa mở cửa</div>
                    </div>
                  </div>
                </a>
              </div>

              <div class="product-item">
                <a href="">
                  <div class="row row-small-space">
                    <div class="col-xs-4">
                      <img src="../img/m-product-detail.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-8">
                      <h2>Chicken Go Saigon Shop Online</h2>
                      <p>83/35 Phạm Văn Bạch, P. 15, Quận Tân Bình, TP.HCM</p>
                      <div class="store-status active">Mở cửa</div>
                    </div>
                  </div>
                </a>
              </div>
              <?php } ?>
            </div>

            <div class="row-footer">
              <a href="" class="btn rounded btn-block">
                <i class="icon-search"></i>
                <span>Tìm thêm kết quả cho <b>"Món"</b></span>
              </a>
            </div>
          </div>

          <div class="search-empty hidden">
            <img src="../img/search-no-result.png" alt="">
            <h4>Không tìm thấy từ khóa liên quan!</h4>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>