    </main>

    <footer>
      <div class="link-utils">
        <a data-toggle="modal" data-target="#modalPaymentGuide">Hướng dẫn thanh toán</a>
        <a data-toggle="modal" data-target="#modalOrderTracking">Tra cứu đơn hàng</a>
        <a data-toggle="modal" data-target="#modalPaymentMethod">Phương thức thanh toán</a>
        <a data-toggle="modal" data-target="#modalAddressList">Sổ địa chỉ</a>
      </div>

      <div class="subscribe">
        <h2 class="f-heading">ĐĂNG KÝ NHẬN TIN</h2>
        <p>Để nhận các thông báo khuyến mại mới nhất</p>
        <div class="input-wrapper">
          <input type="text" placeholder="Nhập email của bạn">
          <a><i class="icon-check"></i></a>
        </div>
      </div>

      <div class="appstore">
        <h2 class="f-heading">TẢI MIỄN PHÍ FOODNOW APP</h2>
        <div class="custom-row">
          <div class="column-2">
            <a href=""><img src="../img/m-apple-store.png" alt="" class="img-responsive center-block"></a>
          </div>
          <div class="column-2">
            <a href=""><img src="../img/m-google-play.png" alt="" class="img-responsive center-block"></a>
          </div>
        </div>
      </div>

      <div class="fb-fanpage">
        <h2 class="f-heading">LIKE ĐỂ NHẬN ƯU ĐÃI MỚI NHẤT</h2>
        <div class="fb-plugin text-center">
          <div class="fb-page" data-href="https://www.facebook.com/facebook" data-width="300" data-hide-cover="true" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
        </div>
      </div>

      <hr>

      <div class="f-info">
        <div>Công Ty Cổ Phần Foodnow Lầu 8, Tòa nhà Jabes 1, 244 Cống Quỳnh, P. Phạm Ngũ Lão, Quận 1, Tp.HCM</div>
        <div>© Copyright 2015. All Rights Reserved.</div>
      </div>
    </footer>
    
    <script src="../lib/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="../lib/OwlCarousel/owl.carousel.min.js"></script>
    <script src="../lib/validator.min.js"></script>
    
    <script src="js/script.js"></script>

  
    <!-- Modal search -->
    <?php include 'modal/search.php'; ?>
  
    <!-- Modal login -->
    <?php include 'modal/login.php'; ?>

    <!-- Modal forgot password -->
    <?php include 'modal/forgot-password.php'; ?>

    <!-- Modal active pin -->
    <?php include 'modal/forgot-password-active-pin.php'; ?>

    <!-- Modal active pin -->
    <?php include 'modal/forgot-password-enter-new.php'; ?>

    <!-- Modal active pin -->
    <?php include 'modal/payment-guide.php'; ?>

    <!-- Modal active pin -->
    <?php include 'modal/change-password.php'; ?>

    <!-- Modal profile -->
    <?php include 'modal/profile.php'; ?>

    <!-- Modal order tracking -->
    <?php include 'modal/order-tracking.php'; ?>

    <!-- Modal order list -->
    <?php include 'modal/order-list.php'; ?>

    <!-- Modal order detail -->
    <?php include 'modal/order-detail.php'; ?>

    <!-- Modal payment method -->
    <?php include 'modal/payment-method.php'; ?>

    <!-- Modal address list -->
    <?php include 'modal/address-list.php'; ?>

    <!-- Modal cart -->
    <?php include 'modal/cart.php' ?>

    <!-- Modal add cart -->
    <?php include 'modal/add-cart.php' ?>

    <!-- Modal cart group -->
    <?php include 'modal/cart-group.php' ?>

    <!-- Modal cart share -->
    <?php include 'modal/share.php' ?>

    <!-- Modal pay -->
    <?php include 'modal/pay.php' ?>
    
    <!-- Modal add address -->
    <?php include 'modal/add-address.php'; ?>
  </body>
</html>