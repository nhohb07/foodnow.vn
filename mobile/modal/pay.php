<div class="modal fade modal-pay modal-cart" id="modalPay" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $modalHeader = 'Thanh toán'; include 'modal-header.php'; ?>

      <div class="modal-body">
        
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="payStep">
          <li role="presentation" class="active">
            <a href="#step-1" aria-controls="step-1" role="tab" data-toggle="tab">1. THÔNG TIN</a>
          </li>
          <li role="presentation">
            <a href="#step-2" aria-controls="step-2" role="tab" data-toggle="tab">2. ĐƠN HÀNG</a>
          </li>
          <li role="presentation">
            <a href="#step-3" aria-controls="step-3" role="tab" data-toggle="tab">3. HOÀN TẤT</a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content scrollable">

          <!-- Scene Buyer -->
          <div role="tabpanel" class="tab-pane fade in active" id="step-1">
            <div id="map-canvas" class="map-container"></div>

            <div class="store-info">
              <h5 class="store-name">Chicken Go Saigon - Shop Online</h5>
              <small class="store-address">
                751/16 Trần Xuân Soạn, P. Tân Hưng, Quận 7, TP. HCM
              </small>
            </div>

            <hr>

            <div class="shipping-time">
              <h5 class="heading">Thời Gian Nhận Hàng</h5>

              <div class="custom-row">
                <div class="input-date-time pull-left">
                  <input type="text" class="form-control no-shadow datepicker" value="<?=date('d/m/Y')?>">
                </div>

                <div class="select-time pull-right">
                  <select class="form-control no-shadow">
                    <option value="">Càng sớm càng tốt</option>
                    <option value="">Càng sớm càng tốt</option>
                  </select>
                </div>
              </div>
            </div>

            <hr>

            <div class="shipping-n-cost">
              <h5 class="heading">Vận chuyển và phí ước tính</h5>
              <small>Khoảng cách: 5,2 km  -  Thời gian: 15 phút</small>
              <small>Phí: 50,000 đ</small>
            </div>

            <hr class="medium">

            <div class="shipping-address">
              <h5 class="heading">Địa Điểm Nhận Hàng</h5>

              <!-- Form add address if address list is unavailable, add/remove class hidden -->
              <div class="form-address-unavailable hidden">
                <form action="" data-toggle="validator" onsubmit="return false;">
                  <!-- Name -->
                  <div class="form-group">
                    <input type="text" class="form-control input-line-bottom" placeholder="Họ Tên" value="Nguyễn Minh Toàn" required data-error="Hãy nhập họ tên">
                    <div class="help-block with-errors"></div>
                  </div>

                  <!-- Phone -->
                  <div class="form-group">
                    <input type="text" class="form-control input-line-bottom" placeholder="Số điện thoại" value="0912345678" required data-error="Hãy nhập số điện thoại">
                    <div class="help-block with-errors"></div>
                  </div>
          
                  <!-- Address -->
                  <div class="form-group">
                    <input type="text" class="form-control input-line-bottom" placeholder="Địa chỉ" required data-error="Hãy nhập địa chỉ">
                    <div class="help-block with-errors"></div>
                  </div>
                  
                  <!-- Note -->
                  <div class="form-group">
                    <input type="text" class="form-control input-line-bottom" placeholder="Ghi chú cho đơn hàng">
                  </div>

                  <button id="form-unavailable-submit" type="submit" class="btn btn-small btn-block btn-green rounded" onclick="submitFormAddressUnavailable()">TIẾP TỤC</button>

                </form>
              </div>

              <!-- Form select address from list available, add/remove class hidden -->
              <div class="form-address-available">
                <form action="" onsubmit="return false;">
                  <ul>
                    <?php for($i = 0; $i < 2; $i++) { ?>
                    <li class="address-item">
                      <label class="radio-wrapper radio-default">
                        <input type="radio" id="address-item-<?=$i?>" name="address" checked>
                        <span></span>
                      </label>
                      <label class="address-item-info" for="address-item-<?=$i?>">
                        <div class="address-item-detail">
                          <div class="">
                            <div class="address-item-name">Nguyễn Minh Toàn</div>
                            <div class="address-item-phone">
                              81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM<br>
                              0969740960
                            </div>
                          </div>
                          <div class="address-item-action">
                            <a class="link link-edit">Chỉnh sửa</a>
                          </div>
                        </div>
                      </label>
                    </li>
                    <?php } ?>

                  </ul>

                  <div class="actions">
                    <a class="link link-edit" data-toggle="modal" data-target="#modalAddAddress">Thêm địa chỉ mới</a>
                  </div>

                  <button type="submit" class="btn btn-small btn-block btn-green rounded" onclick="nextStep(2)">TIẾP TỤC</button>
                </form>
              </div>
            </div>

          </div>

          <!-- Scene Order Detail -->
          <div role="tabpanel" class="tab-pane fade" id="step-2">

            <div class="section-shipping-address">
              <h5 class="heading">Địa chỉ nhận hàng</h5>
              <div class="buyer-info">
                <div class="buyer-name">Nguyễn Minh Toàn</div>
                <div class="buyer-address">81/13 Hoàng Hoa Thám, p6, Bình Thạnh, HCM</div>
                <div class="buyer-phone">0969740960</div>
              </div>
            </div>

            <div class="section-payment-method">
              <h5 class="heading">Phương thức thanh toán</h5>
              <ul class="payment-method-list">
                <li>
                  <label>
                    <input type="radio" name="payment_method" checked>
                    <span>Tiền mặt khi nhận hàng</span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="radio" name="payment_method">
                    <span>Internet Banking</span>
                  </label>
                </li>
                <li>
                  <label>
                    <input type="radio" name="payment_method">
                    <span>Creadit Visa / MASTER / JCB</span>
                  </label>
                </li>
              </ul>
            </div>

            <hr>

            <div class="section-product cart-body">
              <h5 class="heading">Chi tiết đơn hàng</h5>
              <div class="scroll-wrapper-1">
                <?php for($u = 0; $u < 2; $u++) { ?>
                <div class="user">
                  <div class="user-avatar"></div>
                  <div class="user-name">Toàn Nguyễn</div>
                  <div class="user-quantity">4 món</div>
                </div>

                <div class="products">
                  <?php for($i = 0; $i < 3; $i++) { ?>
                  <div class="product-item">
                    <div class="product-row">
                      <div class="product-quantity">
                        <span class="quantity-button quantity-increase"><i class="icon-plus"></i></span>
                        <span class="quantity-text">1</span>
                        <span class="quantity-button quantity-decrease"><i class="icon-minus"></i></span>
                      </div>
                      <div class="product-name">Gà ta bó xôi chà bông</div>
                    </div>
                    <div class="product-row">
                      <div class="product-note">
                        <input type="text" placeholder="Ghi chú">
                      </div>
                      <div class="product-price">30,000 đ</div>
                    </div>
                  </div>
                  <?php } ?>

                </div>
                <?php } ?>
              </div>

              <div class="cart-footer">
                <div class="cart-footer-row cart-total">
                  <div class="pull-left">Cộng</div>
                  <div class="pull-right"><b>120,000 đ</b></div>
                </div>
                <div class="cart-footer-row cart-shipping">
                  <div class="pull-left">Phí vận chuyển (Est.)</div>
                  <div class="pull-right"><span>5,000 đ/km</span></div>
                </div>
                <div class="cart-footer-row input-coupon-code">
                  <span>Nhập mã khuyến mãi</span>
                  <input type="text" placeholder="NHẬP MÃ" class="is-empty">
                </div>
                <div class="cart-footer-row cart-estimate">
                  <div class="pull-left">Tạm tính</div>
                  <div class="pull-right"><b class="text-red">125,000 đ</b></div>
                </div>
                <div class="cart-button">
                  <div class="row row-small-space">
                    <div class="col-xs-6">
                      <a class="btn btn-block btn-small btn-other rounded" data-dismiss="modal">hủy</a>
                    </div>
                    <div class="col-xs-6">
                      <a 
                        class="btn btn-block btn-small rounded btn-green"
                        onclick="nextStep(3)"
                      >TIẾP TỤC</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <!-- Scene Congration -->
          <div role="tabpanel" class="tab-pane fade" id="step-3">
            <h2>XIN CHÚC MỪNG!</h2>
            <p class="text-center">
              Các nguyên liệu đã được sẳn sàng để chế biến, Thượng Đế hãy chờ trong chốt lát ạ!
            </p>

            <div class="actions">
              <a class="btn rounded btn-block btn-small btn-other">THEO DÕI ĐƠN HÀNG</a>
              <a class="btn rounded btn-block btn-small btn-green">TRANG CHỦ</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script>
  $('#payStep li a').click(function(e) {
    e.stopPropagation();
  });

  function nextStep(step) {
    $('#payStep li:eq('+(step - 1)+') a').tab('show')
  }

  function submitFormAddressUnavailable() {
    if(!$('#form-unavailable-submit').hasClass('disabled')) {
      nextStep(2);
    }
  }

  // init google map
  function initialize() {
    var map_canvas = document.getElementById('map-canvas');
    var map_options = {
      center: new google.maps.LatLng(10.762622, 106.660172),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options)
  }
  google.maps.event.addDomListener(window, 'load', initialize);

  $('#modalPay').on('show.bs.modal',function(){
    //reset to step 1 after modal open
    nextStep(1);
  });

  $('#modalPay').on('shown.bs.modal',function(){
    //reinit modal after modal show
    initialize();
  });

</script>